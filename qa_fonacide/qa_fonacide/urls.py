from django.conf.urls import patterns, include, url
from django.contrib import admin

from recepcion_xls.views import RedirectRegistrationView

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'qa_fonacide.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^',include('recepcion_xls.urls',namespace='recepcion_xls',app_name='recepcion_xls')),
    url(r'^accounts/register/$', RedirectRegistrationView.as_view(), name='registration_register'),
    url(r'^accounts/', include('registration.backends.simple.urls'))
)
