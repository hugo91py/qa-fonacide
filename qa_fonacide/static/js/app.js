

// Submit post on submit


// AJAX for posting
function create_post() {
    console.log("create post is working!") // sanity check
  	 $.ajax({
        url : "subir_archivo/", // the endpoint
        type : "POST", // http method
        data : { the_post : $('#post-text').val() }, // data sent with the post request

        // handle a successful response
        success : function(json) {
            $('#post-text').val(''); // remove the value from the input
            console.log(json); // log the returned json to the console
            console.log("success"); // another sanity check
        },

        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            $('#results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+errmsg+
                " <a href='#' class='close'>&times;</a></div>"); // add the error to the dom
            console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
        }
    });  
};


//Token CSRF
// This function gets cookie with a given name
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');
 
/*
The functions below will create a header with csrftoken
*/
 
function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
function sameOrigin(url) {
    // test that a given url is a same-origin URL
    // url could be relative or scheme relative or absolute
    var host = document.location.host; // host + port
    var protocol = document.location.protocol;
    var sr_origin = '//' + host;
    var origin = protocol + sr_origin;
    // Allow absolute or scheme relative URLs to same origin
    return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
        (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
        // or any other URL that isn't scheme relative or absolute i.e relative.
        !(/^(\/\/|http:|https:).*/.test(url));
}
 
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
            // Send the token to same-origin, relative URLs only.
            // Send the token only if the method warrants CSRF protection
            // Using the CSRFToken value acquired earlier
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

//Para tabla de sugerencias
$("#TablaSugerencias tr").click(function(){
   $(this).addClass('info').siblings().removeClass('info');    
   var institucion=$(this).find('td:nth-child(1)').html();
   $("#id_codigo_institucion").val(institucion)
   var local =$(this).find('td:nth-child(2)').html();
   $("#id_codigo_local").val(local)
   
   //console.log(institucion);
    //console.log(local)    
});

//Para input de fechas
var year = new Date().getFullYear()
$('#datepicker').val(year)

//Para descargar archivo de errores

function saveTextAsFile()
{
    var textToWrite = document.getElementById("textoError").value;
    var textFileAsBlob = new Blob([textToWrite], {type:'text/plain'});
    
    var downloadLink = document.createElement("a");
    downloadLink.download = "errores.txt";
    downloadLink.innerHTML = "Descargar Archivo";
    if (window.webkitURL != null)
    {
        // Chrome allows the link to be clicked
        // without actually adding it to the DOM.
        downloadLink.href = window.webkitURL.createObjectURL(textFileAsBlob);
    }
    else
    {
        // Firefox requires the link to be added to the DOM
        // before it can be clicked.
        downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
        downloadLink.onclick = destroyClickedElement;
        downloadLink.style.display = "none";
        document.body.appendChild(downloadLink);
    }

    downloadLink.click();
}

function destroyClickedElement(event)
{
    document.body.removeChild(event.target);
}

$( document ).ready(function() {
                $("#datepicker").datepicker( {
                    format: " yyyy",
                    viewMode: "years", 
                    minViewMode: "years",
                    onSelect: function(d,i){
                              if(d !== i.lastVal){
                                  $(this).change();
                              }
                         }
                });
                });



function sLoadingEstadoPlanillas() {
    var spinner = new Spinner({
        color: "#246BB2",
        radius: 13,
        width: 10,

        length: 10
    }).spin();
    $("#loader-estado").removeClass().append(spinner.el);
}

function fLoadingEstadoPlanillas() {
    // first, toggle the class 'done', which makes the loading screen
    // fade out
    var loader = $("#loader-estado");
    loader.addClass('done');
    setTimeout(function() {
        // then, after a half-second, add the class 'hide', which hides
        // it completely and ensures that the user can interact with the
        // map again.
        loader.addClass('hide');
        loader.empty();
    }, 200);
}