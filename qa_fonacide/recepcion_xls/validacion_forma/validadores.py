#coding: UTF-8

#Conversion de Acentos
import sys 
reload(sys) 
sys.setdefaultencoding("utf-8")
import unicodedata

#estilos
from recepcion_xls.constantes import rojo,amarillo,verde, lista_departamentos


###Funciones Auxiliares###
def remover_acentos(input_str):
	nkfd_form = unicodedata.normalize('NFKD', unicode(input_str))
	return u"".join([c for c in nkfd_form if not unicodedata.combining(c)])

#validaciones

#respuesta {valido:True|False, marca_color:estilo excel, motivo:texto que explica el error}
#
def es_nro_prioridad(entrada):
	return es_numero(entrada,natural=True)

def es_departamento(entrada):
	if es_cadena(entrada)["valido"]:
		entrada = remover_acentos(entrada)
		entrada = entrada.strip().lower()
		if entrada in lista_departamentos:
			return {"valido":True}
		else:
			return {"valido":False, "estilo":rojo, "motivo":"El valor no es un nombre de departamento válido"}
	else:
		return {"valido":False, "estilo":rojo, "motivo":"El valor no es una cadena válida"}

def es_cod_local(entrada):
	#if type(entrada) == "str":
	entrada = str(entrada)
	entrada = entrada.lower()
	entrada = entrada.strip()
	entrada = entrada.replace("o","0")

	validacion_nro = es_numero(entrada)
	if validacion_nro["valido"]:
		entrada = str(entrada).split(".")[0]
		return es_cadena(entrada,longitud_fija=7)
	else:
		return validacion_nro

def es_cod_inst(entrada): 
	cadena = es_cadena(entrada,puede_ser_vacio=False)
	if cadena["valido"]:
		return es_numero(entrada)
	else:
		return cadena 

#Campo Este,Norte
def es_coordenada_cartesiana(entrada):
	return {"valido":True}
#Campo Longitud Latitud
def es_coordenada_geo(entrada):
	return {"valido":True}
def es_nom_inst(entrada):
	return es_cadena(entrada,puede_ser_vacio=False)
def es_distrito(entrada):
	return es_cadena(entrada,puede_ser_vacio=False)
def es_localidad_barrio(entrada):
	return es_cadena(entrada,puede_ser_vacio=False)
def es_zona(entrada):
	if entrada.strip().lower() in ["ur.", "ur", "u", "urb", "urb.","urbano"]:
		return {"valido":False, "estilo":verde, "motivo":"El campo solo puede contener los valores \"Rural\" o \"Urbana\" " , "correccion":"Urbana"}
	if entrada.strip().lower() in ["ru.", "ru", "r"]:
		return {"valido":False, "estilo":verde, "motivo":"El campo solo puede contener los valores \"Rural\" o \"Urbana\" " , "correccion":"Rural"}
	if entrada.strip().lower() in ["rural","urbana"]:
		return {"valido":True}
	else:
		return {"valido":False, "estilo":rojo, "motivo":"El campo solo puede contener los valores \"Rural\" o \"Urbana\" " }
		
def es_asentamiento_colonia(entrada):
	return es_cadena(entrada)
def es_nro_beneficiados(entrada):
	return es_numero(entrada)
def es_nivel_educativo(entrada):
	return es_cadena(entrada)
#Nueva,Reparar,Adecuar
def es_nro_construcciones(entrada):
	return es_numero(entrada)
def es_abastecimiento_agua(entrada):
	return es_cadena(entrada)
def es_suministro_energia(entrada):
	return es_cadena(entrada)
def es_servicio_sanitario(entrada):
	return es_cadena(entrada)
def es_titulo_propiedad(entrada):
	return es_cadena(entrada)
def es_cuenta_con_plano(entrada):
	return es_cadena(entrada)
def es_cuenta_construccion(entrada):
	return es_cadena(entrada)
def es_cuenta_con_mecanismos_prevencion(entrada):
	return es_cadena(entrada)
def es_justificacion(entrada):
	return es_cadena(entrada,puede_ser_vacio=False)


#Planilla Aula
#Planilla Sanitarios
#Planilla Inmobiliarios
#Planilla Otros
#Planilla Alimentos


#Genericos
def es_numero(entrada, natural=False):
	entrada = str(entrada)
	entrada = entrada.strip()
	try:
		#Reemplazar por un regex
		numero = float(entrada)
		if natural:
			if numero < 0:
				return {"valido":False, "estilo":rojo, "motivo":"El valor es menor a 0 "}
		return {"valido":True} 
	except:
		return {"valido":False,"estilo":rojo,"motivo":"El valor no es un número "}

def es_cadena(entrada, puede_ser_vacio=True, longitud_fija=False, longitud_max=False):
	
	#Chequear que sea una cadena
	try:
		entrada = str(entrada)
	except:
		return {"valido":False,"estilo":rojo,"motivo":"El valor no es una cadena válida "}


	#Chequear que no sea vacia
	if not puede_ser_vacio:
		if entrada == "":
			return {"valido":False,"estilo":amarillo,"motivo":"El campo debe contener algún valor "}

	#Chequear que no supere una cierta longitud
	if longitud_max:
		if len(entrada)>longitud_max:
			return {"valido":False,"estilo":rojo,"motivo":"El valor sobrepasa la longitud máxima de "+str(longitud_max)+" caracteres "}

	#Chequear que tenga exactamente cierta longitud
	if longitud_fija:
		if len(entrada)>longitud_fija or len(entrada)<longitud_fija:
			return {"valido":False,"estilo":rojo,"motivo":"El valor debe tener una longitud de "+str(longitud_fija)+" caracteres "}

	
	if entrada.strip().lower() in ["_","—","–","---","","(-)","(_)","(—)","(–)"]:
		return {"valido":False,"estilo":verde,"motivo":"El valor correcto para marcar en blanco una casilla es - (guión)","correccion":"-"}
	
	return {"valido":True}

