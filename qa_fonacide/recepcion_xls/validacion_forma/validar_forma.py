#coding: UTF-8
#Script que valida el formato

#tempfiles
import tempfile
import os

from numbers import Number

import codecs #para escribir en UTF-8

import re #Para soporte de regex

import string
from fuzzywuzzy import fuzz

#Conversion de Acentos
import sys 
reload(sys) 
sys.setdefaultencoding("utf-8")
import unicodedata

#Enviar el stream del archivo a la vista
import StringIO

from recepcion_xls import constantes

#Excel
import xlrd
import xlwt

from xlrd import open_workbook
from xlutils.copy import copy


#validadores
import validadores
from recepcion_xls.validacion_fondo import validar_fondo
#obtener modelos
from recepcion_xls import models

##################################################
class ValidadorColumna():
	"""
	Clase que permite definir un objeto  para validar una columna.
	Esta compuesta por:
	-Funcion de validacion: Retorna si el valor en la columna cumple las reglas de validacion para esa columna
	-Funcion de reparacion: Reemplaza valores como "-","_" por el valor que corresponda en la columna. (No es obligatorio)
	-Nombre del Atributo: El nombre del atributo al que debe ser mapeado este valor en el objeto que modela la tabla de la BD.
	"""
	func_de_validacion = None
	nombre_atributo = ""
	func_de_reparacion = None
	
	def __init__(self,func_de_validacion,func_de_reparacion,nombre_atributo):
		self.func_de_validacion = func_de_validacion
		self.func_de_reparacion = func_de_reparacion
		self.nombre_atributo = nombre_atributo

	def validar(self,valor):
		return self.func_de_validacion(valor)

	def reparar(self,valor):
		return self.func_de_reparacion(valor)

####################Constantes####################
#[A...Z,AA,...AZ]
EXCEL_COLUMN = [chr(value).upper() for value in range(97,97+26)] + \
				["A"+chr(value).upper() for value in range(97,97+26)]
#LISTO
cabecera_aulas = {	u"Nº de Prioridad":[validadores.es_nro_prioridad,"numero_prioridad"],
		u"Código de local":[validadores.es_cod_local,"codigo_local"],
		u"Código de Institución":[validadores.es_cod_inst,"codigo_institucion"],
		u"Este":[validadores.es_coordenada_cartesiana,"distancia_este"],
		u"Norte":[validadores.es_coordenada_cartesiana,"distancia_norte"],
		u"Latitud":[validadores.es_coordenada_geo,"latitud"],
		u"Longitud":[validadores.es_coordenada_geo,"longitud"],
		u"Nombre de la Institución":[validadores.es_nom_inst,"nombre_institucion"],
		u"Distrito":[validadores.es_distrito,"distrito"],
		u"Localidad o Barrio":[validadores.es_localidad_barrio,"localidad"],
		u"Zona":[validadores.es_zona,"zona"],
		u"Nombre de Asentamiento o Colonia":[validadores.es_asentamiento_colonia,"asentamiento_colonia"],
		u"Nivel educativo beneficiado":[validadores.es_nivel_educativo,"nivel_educativo_beneficiado"],
		u"Abastecimiento de Agua":[validadores.es_abastecimiento_agua,"abastecimiento_agua"],
		u"Suministro de Energía Eléctrica":[validadores.es_suministro_energia,"suministro_energia_electrica"],
		u"Servicio sanitario que dispone actualmente":[validadores.es_servicio_sanitario,"servicio_sanitario"],
		u"Título de propiedad":[validadores.es_titulo_propiedad,"titulo_propiedad"],
		u"Cuenta con plano":[validadores.es_cuenta_con_plano,"cuenta_con_plano"],
		u"Justificación del requerimiento":[validadores.es_justificacion,"justificacion"],
		u"Cuenta con mecanismos de prevenciòn de incendio":[validadores.es_cuenta_con_mecanismos_prevencion,"cuenta_mecanismo_prevencion_incendio"],	

		u"Construcción nueva":[validadores.es_nro_construcciones,"construccion_nueva_cantidad"],
		u"Construcción a reparar":[validadores.es_nro_construcciones,"construccion_reparar_cantidad"],
		u"Construcción a adecuar":[validadores.es_nro_construcciones,"construccion_reparar_cantidad"],
		

		u"Nro de beneficiados":[validadores.es_nro_beneficiados,"numero_beneficiados"],
		u"Cuenta con espacio para construcción":[validadores.es_cuenta_construccion,"cuenta_con_espacio_para_construccion"],
		
		#u"Departamento":[validadores.es_departamento,"departamento"],
		}
#LISTO
cabecera_inmobiliario = { 
		u"Nº de Prioridad":[validadores.es_nro_prioridad,"numero_prioridad"],
		u"Código de local":[validadores.es_cod_local,"codigo_local"],
		u"Código de Institución":[validadores.es_cod_inst,"codigo_institucion"],
		u"Este":[validadores.es_coordenada_cartesiana,"distancia_este"],
		u"Norte":[validadores.es_coordenada_cartesiana,"distancia_norte"],
		u"Latitud ":[validadores.es_coordenada_geo,"latitud"],
		u"Longitud":[validadores.es_coordenada_geo,"longitud"],
		u"Nombre de la Institución":[validadores.es_nom_inst,"nombre_institucion"],
		u"Distrito":[validadores.es_distrito,"distrito"],
		u"Localidad o Barrio":[validadores.es_localidad_barrio,"localidad"],
		u"Zona":[validadores.es_zona,"zona"],
		u"Nombre de Asentamiento o Colonia":[validadores.es_asentamiento_colonia,"asentamiento_colonia"],
		u"Nivel educativo beneficiado":[validadores.es_nivel_educativo,"nivel_educativo_beneficiado"],
		u"Abastecimiento de Agua":[validadores.es_abastecimiento_agua,"abastecimiento_agua"],
		u"Suministro de Energía Eléctrica":[validadores.es_suministro_energia,"suministro_energia_electrica"],
		u"Servicio sanitario que dispone actualmente":[validadores.es_servicio_sanitario,"servicio_sanitario"],
		u"Título de propiedad":[validadores.es_titulo_propiedad,"titulo_propiedad"],
		u"Cuenta con plano":[validadores.es_cuenta_con_plano,"cuenta_con_plano"],
		u"Justificación del requerimiento":[validadores.es_justificacion,"justificacion"],
		u"Cuenta con mecanismos de prevenciòn de incendio":[validadores.es_cuenta_con_mecanismos_prevencion,"cuenta_mecanismo_prevencion_incendio"],
		u"Sillas o pupitres|Cantidad":[validadores.es_nro_construcciones,"silla_cantidad"],
		u"Sillas o pupitres|Nro. de beneficiados":[validadores.es_nro_beneficiados,"sillas_beneficiados"],
		u"Pizarrón|Cantidad":[validadores.es_nro_construcciones,"pizarron_cantidad"],
		u"Pizarrón|Nro. de beneficiados":[validadores.es_nro_beneficiados,"pizarron_beneficiados"],
		u"Otros|Cantidad":[validadores.es_nro_construcciones,"otros_cantidad"],
		u"Otros|Nro. de beneficiados":[validadores.es_nro_beneficiados,"otros_beneficiados"],
		

		#u"Departamento":[validadores.es_departamento,"departamento"],
		

		}
#LISTO
cabecera_otros = { u"Nº de Prioridad":[validadores.es_nro_prioridad,"numero_prioridad"],
		u"Código de local":[validadores.es_cod_local,"codigo_local"],
		u"Código de Institución":[validadores.es_cod_inst,"codigo_institucion"],
		u"Este":[validadores.es_coordenada_cartesiana,"distancia_este"],
		u"Norte":[validadores.es_coordenada_cartesiana,"distancia_norte"],
		u"Latitud ":[validadores.es_coordenada_geo,"latitud"],
		u"Longitud":[validadores.es_coordenada_geo,"longitud"],
		u"Nombre de la Institución":[validadores.es_nom_inst,"nombre_institucion"],
		u"Distrito":[validadores.es_distrito,"distrito"],
		u"Localidad o Barrio":[validadores.es_localidad_barrio,"localidad"],
		u"Zona":[validadores.es_zona,"zona"],
		u"Nombre de Asentamiento o Colonia":[validadores.es_asentamiento_colonia,"asentamiento_colonia"],
		u"Nro de beneficiados":[validadores.es_nro_beneficiados,"numero_beneficiados"],
		u"Nivel educativo beneficiado":[validadores.es_nivel_educativo,"nivel_educativo_beneficiado"],
		
		u"Cantidad":[validadores.es_nro_construcciones,"construccion_nueva_cantidad"],
		u"Especificar espacio":[validadores.es_justificacion,"construccion_nueva_espacio"],
		u"Cantidad":[validadores.es_nro_construcciones,"construccion_reparar_cantidad"],
		u"Especificar espacio":[validadores.es_justificacion,"construccion_reparar_espacio"],
		u"Cantidad":[validadores.es_nro_construcciones,"construccion_adecuar_cantidad"],
		u"Especificar espacio":[validadores.es_justificacion,"construccion_adecuar_espacio"],
		
		u"Abastecimiento de Agua":[validadores.es_abastecimiento_agua,"abastecimiento_agua"],
		u"Suministro de Energía Eléctrica":[validadores.es_suministro_energia,"suministro_energia_electrica"],
		u"Servicio sanitario que dispone actualmente":[validadores.es_servicio_sanitario,"servicio_sanitario"],
		u"Título de propiedad":[validadores.es_titulo_propiedad,"titulo_propiedad"],
		u"Cuenta con plano":[validadores.es_cuenta_con_plano,"cuenta_con_plano"],
		u"Cuenta con espacio para construcción":[validadores.es_cuenta_construccion,"cuenta_con_espacio"],
		u"Cuenta con mecanismos de prevenciòn de incendio":[validadores.es_cuenta_con_mecanismos_prevencion,"cuenta_mecanismo_prevencion_incendio"],
		u"Justificación del requerimiento":[validadores.es_justificacion,"justificacion"],
		
		#u"Departamento":[validadores.es_departamento,"departamento"],
		
		}
#LISTO
cabecera_sanitarios = {	u"Nº de Prioridad":[validadores.es_nro_prioridad,"numero_prioridad"],
		
		u"Código de local":[validadores.es_cod_local,"codigo_local"],
		u"Código de Institución":[validadores.es_cod_inst,"codigo_institucion"],
		u"Este":[validadores.es_coordenada_cartesiana,"distancia_este"],
		u"Norte":[validadores.es_coordenada_cartesiana,"distancia_norte"],
		u"Latitud ":[validadores.es_coordenada_geo,"latitud"],
		u"Longitud":[validadores.es_coordenada_geo,"longitud"],
		u"Nombre de la Institución":[validadores.es_nom_inst,"nombre_institucion"],
		u"Distrito":[validadores.es_distrito,"distrito"],
		u"Localidad o Barrio":[validadores.es_localidad_barrio,"localidad"],
		u"Zona":[validadores.es_zona,"zona"],
		u"Nombre de Asentamiento o Colonia":[validadores.es_asentamiento_colonia,"asentamiento_colonia"],
		u"Nro de beneficiados":[validadores.es_nro_beneficiados,"numero_beneficiados"],
		u"Nivel educativo beneficiado":[validadores.es_nivel_educativo,"nivel_educativo_beneficiado"],
		u"Construcción nueva":[validadores.es_nro_construcciones,"construccion_nueva_cantidad"],
		u"Construcción a reparar":[validadores.es_nro_construcciones,"construccion_reparar_cantidad"],
		u"Construcción a adecuar":[validadores.es_nro_construcciones,"construccion_adecuar_cantidad"],
		u"Abastecimiento de Agua":[validadores.es_abastecimiento_agua,"abastecimiento_agua"],
		u"Suministro de Energía Eléctrica":[validadores.es_suministro_energia,"suministro_energia_electrica"],
		u"Servicio sanitario que dispone actualmente":[validadores.es_servicio_sanitario,"servicio_sanitario"],
		u"Título de propiedad":[validadores.es_titulo_propiedad,"titulo_propiedad"],
		u"Cuenta con plano":[validadores.es_cuenta_con_plano,"cuenta_con_plano"],
		u"Cuenta con espacio para construcción":[validadores.es_cuenta_construccion,"cuenta_con_espacio_para_construccion"],
		u"Cuenta con mecanismos de prevenciòn de incendio":[validadores.es_cuenta_con_mecanismos_prevencion,"cuenta_mecanismo_prevencion_incendio"],
		u"Justificación del requerimiento":[validadores.es_justificacion,"justificacion"]
		
		#u"Departamento":[validadores.es_departamento,"departamento"],

		}
#LISTO

cabecera_alimentos = { 
		u"Nº de Prioridad":[validadores.es_nro_prioridad,"numero_prioridad"],
		u"Código de local":[validadores.es_cod_local,"codigo_local"],
		u"Código de Institución":[validadores.es_cod_inst,"codigo_institucion"],
		u"Este":[validadores.es_coordenada_cartesiana,"distancia_este"],
		u"Norte":[validadores.es_coordenada_cartesiana,"distancia_norte"],
		u"Latitud ":[validadores.es_coordenada_geo,"latitud"],
		u"Longitud":[validadores.es_coordenada_geo,"longitud"],
		u"Nombre de la Institución":[validadores.es_nom_inst,"nombre_institucion"],
		u"Localidad o Barrio":[validadores.es_localidad_barrio,"localidad"],
		u"Zona":[validadores.es_zona,"zona"],
		u"Nombre de Asentamiento o comunidad indígena":[validadores.es_asentamiento_colonia,"asentamiento_colonia"],
		u"M":[validadores.es_nro_beneficiados,"numero_beneficiados_m"],
		u"F":[validadores.es_nro_beneficiados,"numero_beneficiados_f"],
		u"Abastecimiento de Agua":[validadores.es_abastecimiento_agua,"abastecimiento_agua"],
		u"Suministro de Energía Eléctrica":[validadores.es_suministro_energia,"suministro_energia_electrica"],
		u"Servicio sanitario que dispone actualmente":[validadores.es_servicio_sanitario,"servicio_sanitario"],
		
		u"Cuenta con espacio para la contrucción de cocina-comedor":[validadores,"cuenta_con_espacio_cocina_comedor"],
		u"Recibe merienda escolar|SI|Leche":[validadores,"recibe_merienda_si_leche"],
		u"Recibe merienda escolar|SI|Alimento sólido":[validadores,"recibe_merienda_si_alimento"],
		u"Recibe merienda escolar|NO":[validadores,"recibe_merienda_no"],
		u"Recibe Almuerzo Escolar|SI|Almuerzo fresco|Servicio de Catering":[validadores,"recibe_almuerzo_si_fresco_catering"],
		u"Recibe Almuerzo Escolar|SI|Almuerzo fresco|Preparado en el local escolar":[validadores,"recibe_almuerzo_si_fresco_preparado"],
		u"Recibe Almuerzo Escolar|SI|Almuerzo fresco|Otros":[validadores,"recibe_almuerzo_si_fresco_otro"],
		u"Recibe Almuerzo Escolar|SI|Sistema de distribución|Servicio de Catering":[validadores,"recibe_almuerzo_si_distribucion_catering"],
		u"Recibe Almuerzo Escolar|SI|Sistema de distribución|Preparado en el local escolar":[validadores,"recibe_almuerzo_si_distribucion_preparado"],
		u"Recibe Almuerzo Escolar|SI|Sistema de distribución|Otros":[validadores,"recibe_almuerzo_si_distribucion_otros"],
		u"Recibe Almuerzo Escolar|NO":[validadores,"recibe_almuerzo_no"],
		u"Cantidad de meses que recibió el Almuerzo":[validadores,"cantidad_meses_recibio_almuerzo"],
		u"0 al 10":[validadores,"ausentismo_0_10"],
		u"11 al 25":[validadores,"ausentismo_11_25"],
		u">25":[validadores,"ausentismo_25"],
		u"Escuelas de doble escolaridad":[validadores,"escuela_doble_escolaridad"],

		
		
		
		u"Justificación del requerimiento  del programa":[validadores.es_justificacion,"justificacion"],
		
		#u"Departamento":[validadores.es_departamento,"departamento"],
		#u"Distrito":[validadores.es_distrito,"distrito"],
		}

cabeceras = {7:cabecera_inmobiliario,9:cabecera_aulas,10:cabecera_sanitarios,11:cabecera_otros,12:cabecera_alimentos}
####################Constantes####################



####################Funciones Auxiliares####################
def reemplazar_simbolos(valor_entrada,tipo):
	if tipo == "Guion":
		return "-"

def remover_acentos(input_str):
	nkfd_form = unicodedata.normalize('NFKD', unicode(input_str))
	return u"".join([c for c in nkfd_form if not unicodedata.combining(c)])


def header_multinivel(hoja,fila,columna,columna_limite,header):
	#Python
	#import pdb;pdb.set_trace()
	niveles = header.split("|")
	#import pdb;pdb.set_trace()
	columna_seleccionada = None
	f = fila
	c = columna
	continuar = True
	for nivel in range(0,len(niveles)):
		if continuar:
			continuar = False
			for col in range(c,hoja.ncols):
				celda = hoja.cell(f,col).value
				if isinstance(celda,basestring) and "---" in celda:
					celda = celda.replace("-","")
					celda = celda.strip()
				if fuzz.ratio(niveles[nivel],celda) > 90:
					
					columna_seleccionada = col
					f = f + 1
					continuar = True
					break
		else:
			return False,""
	if continuar:
		return True, columna_seleccionada
	else:
		return False,""

def es_header(planilla,cabecera_nombre,hoja,fila,columna):
	"""
	Verifica si el contenido de la celda corresponde al de un header.
	"""

	"""
	Usar | para indicar niveles en las cabeceras, ej:
	cabecera_subitem_subitem entonces si coincide con la cabecera, se baja intenta encajar
	el lugar donde deberia estar el subitem y se guarda entonces esa cabecera
	"""
	#print "es_header"
	#print "Fila: ",fila

	celda = str(hoja.cell(fila,columna).value)
	celda = celda.split("(")[0]
	celda = celda.strip().lower()
	match_ratio_actual = 97
	cabecera_seleccionada = False
	celdas_unidas = hoja.merged_cells
	celdas_unidas = {celdau[2]:celdau[3] for celdau in celdas_unidas}

	if "|" in cabecera_nombre: #Tengo una cabecera multinivel 
		if columna in celdas_unidas.keys(): #Estoy en una columna con celdas combinadas
			#Diferente procesamiento
			columna_limite = celdas_unidas[columna]
			resultado,rcol = header_multinivel(hoja,fila,columna,columna_limite,cabecera_nombre)
			if resultado:
				return cabecera_nombre,rcol
			else:
				return False,""
		else:
			return False,""
	else: #Es una cabecera de un solo nivel, y se procesa normalmente
		fuzzy_ratio = fuzz.ratio(celda,cabecera_nombre.strip().lower())
		if  fuzzy_ratio > 97 and fuzzy_ratio > match_ratio_actual:
			match_ratio_actual = fuzzy_ratio
			cabecera_seleccionada = cabecera_nombre

	return cabecera_seleccionada, columna

def iguales(valor1,valor2):
	valor1Limpio = remover_acentos(valor1)
	valor2Limpio = remover_acentos(valor2)
	valor1Limpio = valor1Limpio.strip().lower()
	valor2Limpio = valor2Limpio.strip().lower()
	
	print "Valor 1: ",valor1Limpio
	print "Valor 2: ",valor2Limpio
	
	return valor1Limpio in valor2Limpio


def transformar(valor):
	if isinstance(valor,basestring):
		return valor.strip() #Eliminamos espacios al inicio o al final
	if isinstance(valor,Number):
		return int(valor) 
	
####################Funciones Auxiliares####################




def leer_xls(archivo,anho,operacion):
	#Se abre el libro en modo lectura, con enconding: UTF-8 y habilitada la lectura de formato
	book = xlrd.open_workbook(archivo,encoding_override="utf-8",formatting_info=True)	
	#En caso de que no pase las validadores se prepara una copia donde si se puede escribir.
	wb = copy(book) 
	
	# Diccionario del tipo {nombre columna:funcion de validacion}
	cabecera = {}
	# Diccionario del tipo {posicion columna:funcion de validacion}
	columnas_func_validacion = {}
	columnas_nombre = {}
	columnas_atributos = {}

	hoja = book.sheets()[0]
	fila_inicio_datos = 0
	planilla_nro = 0
	#Obtenemos el numero de planilla
	for row in range(hoja.nrows):
		for col in range(hoja.ncols):
			cadena = str(hoja.cell(row,col).value).strip().lower()
			
			if "planilla n" in cadena:
				try:
					planilla_nro = int(str(hoja.cell(row,col).value).strip()[-2:])
					if planilla_nro in [7,9,10,11,12]:
						cabecera = cabeceras[planilla_nro]
					#No se puede reconocer el número de planilla
					else:
						return {"lectura":False, 'razon':"No se reconoce el número de Planilla. Favor verifique que la planilla cumpla el formato. Puede utilizar las plantillas del menú a la izquierda para tal efecto."}
				#No se puede reconocer el número de planilla
				except:
					return {"lectura":False, 'razon':"No se reconoce el número de Planilla. Favor verifique que la planilla cumpla el formato. Puede utilizar las plantillas del menú a la izquierda para tal efecto."}
		if planilla_nro != 0:
			break
	

	if planilla_nro == 0:
		return {"lectura":False, 'razon':"No se reconoce el número de Planilla. Favor verifique que la planilla cumpla el formato. Puede utilizar las plantillas del menú a la izquierda para tal efecto."}
	
	distrito = ""
	if planilla_nro == 12:
		for row in range(hoja.nrows):
			for col in range(hoja.ncols):
				cadena = str(hoja.cell(row,col).value).strip().lower()
				
				if "distrito" in cadena:
					
					separado = cadena.split(":")
					if len(separado) == 2:
						distrito = separado[1].strip().lower()
					else:
						return {"lectura":False, 'razon':"No se encuentra el nombre del Distrito. Favor verifique que la planilla cumpla el formato. Puede utilizar las plantillas del menú a la izquierda para tal efecto."}
			if distrito != "":
				break
		if distrito != "":
			distrito = distrito[0].upper() + distrito[1:] #Capitalizar primera letra
		else:
			return {"lectura":False, 'razon':"No se encuentra el nombre del Distrito. Favor verifique que la planilla cumpla el formato. Puede utilizar las plantillas del menú a la izquierda para tal efecto."}


	#Obtenemos el departamento (Comun para las planillas de Infraestructura y Alimentacion)
	departamento = ""
	for row in range(hoja.nrows):
		for col in range(hoja.ncols):
			cadena = str(hoja.cell(row,col).value).strip().lower()
			
			if "departamento" in cadena:
				
				separado = cadena.split(":")
				if len(separado) == 2:
					departamento = separado[1].strip().lower()
				else:
					return {"lectura":False, 'razon':"No se encuentra el nombre del Departamento. Favor verifique que la planilla cumpla el formato. Puede utilizar las plantillas del menú a la izquierda para tal efecto."}
		if departamento != "":
			break
	if departamento != "":
		departamento = departamento[0].upper() + departamento[1:] #Capitalizar primera letra
	else:
		return {"lectura":False, 'razon':"No se encuentra el nombre del Departamento. Favor verifique que la planilla cumpla el formato. Puede utilizar las plantillas del menú a la izquierda para tal efecto."}
	

	#Buscar posicion de los headers y asignarle el tipo
	#de dato correspondiente
	for nombre_cabecera in cabecera:
	#nombre_cabecera = u"Sillas o pupitres_Nro. de beneficiados"
	#print "Buscando Cabecera: ",nombre_cabecera
		for row in range(hoja.nrows):
			
			#if "_" in nombre_cabecera: print "multinivel: ",nombre_cabecera," fila: ",row
			for col in range(hoja.ncols):
				#print nombre_cabecera, hoja.cell(row,col).value
				nombre_columna, columna = es_header(planilla_nro,nombre_cabecera,hoja,row,col)
				#Si hubo un match con el nombre de la columna y la misma no fue previamente
				#seleccionada
				if nombre_columna and nombre_columna != "" and columna not in columnas_func_validacion.keys():
					try:
						#print columna
						#print nombre_columna
						#Asociamos cada numero de columna con una funcion de validacion
						columnas_func_validacion[columna]=cabecera[nombre_columna][0]
						#Asociamos cada numero de columna con el nombre de dicha columna
						columnas_nombre[columna]=nombre_columna
						#Asociamos cada numero de columna con el nombre del atributo en el objeto
						columnas_atributos[columna]=cabecera[nombre_columna][1]
						#Asociamos una funcion de correccion (en caso de que exista)
						#columnas_correccion=cabecera[nombre_columna][2]
						
					except:
						import pdb;pdb.set_trace()
						print "El nombre de cabecera no coincide con el almacenado"

	


	#Un chequeo para verificar que el documento tiene todas las columnas
	if len(columnas_func_validacion.keys()) != len(cabecera.values()):
		
		for key in columnas_nombre.keys():
			pass
			#print key, columnas_nombre[key]
		
		#print "Tamaño Cabecera",len(cabecera.keys())
		#print "Tamaño Cabecera Planilla: ",len(columnas_func_validacion.keys())
		

		return {'lectura':False, 'razon':"La planilla no tiene todas las columnas requeridas. Favor verifique que la planilla cumpla el formato. Puede utilizar las plantillas del menú a la izquierda para tal efecto."}
	


	#Inicio datos
	nombres_columna = {v: k for k, v in columnas_nombre.items()}
	columna_nro_prioridad = nombres_columna[u"Nº de Prioridad"]
	for row in range(hoja.nrows):
		if isinstance(hoja.cell(row,columna_nro_prioridad).value,Number):
			####Cambiar la deteccion de fila de inicio
			####Cambiar la deteccion de fila de fin
			fila_inicio_datos = row
			#tamanho_cabecera = max([v[1]-v[0] for v in hoja.merged_cells ]) #Obtenemos cuantas filas ocupa la cabecera
			#fila_inicio_datos = fila_inicio_datos + tamanho_cabecera - 1 #Obtener donde realmente comienza los datos
			print "Fila donde inician los datos",fila_inicio_datos
			break

	#Buscar fila donde terminan los datos
	fila_fin_datos = hoja.nrows
	
	#import pdb;pdb.set_trace()
	
	for row in range(fila_inicio_datos,hoja.nrows):
		if not isinstance(hoja.cell(row,columna_nro_prioridad).value,Number):
			fila_fin_datos = row
			break




	
	#Chequeamos si ya fue cargada previamente la planilla
	#departamento_id,departamento 
	departamento_obj = models.obtenerDptoPorNombre(departamento)
	distrito_obj = None
	filas_existentes = 0
	print "Distrito?","Distrito" in nombres_columna.keys()
	print nombres_columna.keys()
	if planilla_nro in [7,9,10,11]:
		for i in range(fila_inicio_datos,hoja.nrows):
			valor = hoja.cell(i,nombres_columna["Distrito"]).value
			if isinstance(valor,basestring) and len(valor) > 0:
				break 
		distrito_obj = models.obtenerDistritoPorNombre(valor,departamento_obj)
		print distrito_obj
		filas_existentes = models.PlanillaInfraestructura.objects.filter(departamento_id=departamento_obj.codigo_jurisdiccion,distrito_id=distrito_obj.codigo_jurisdiccion,nro_planilla=planilla_nro,anho=anho).count()
	elif planilla_nro == 12:
		distrito_obj = models.obtenerDistritoPorNombre(distrito,departamento_obj)
		filas_existentes = models.PlanillaAlimentacion.objects.filter(departamento_id=departamento_obj.codigo_jurisdiccion,distrito_id=distrito_obj.codigo_jurisdiccion,nro_planilla=planilla_nro,anho=anho).count()
	#Se ha podido detectar el formato de la planilla
	
	if filas_existentes == 0 or operacion=="sobreescribir" or operacion=="regenerar_planilla":
		if operacion=="sobreescribir":
			if planilla_nro == 12:
			#borrar filas
				filas = models.PlanillaAlimentacion.objects.filter(departamento_id=departamento_obj.codigo_jurisdiccion,distrito_id=distrito_obj.codigo_jurisdiccion,nro_planilla=planilla_nro,anho=anho)
			else:
				filas = models.PlanillaInfraestructura.objects.filter(departamento_id=departamento_obj.codigo_jurisdiccion,distrito_id=distrito_obj.codigo_jurisdiccion,nro_planilla=planilla_nro,anho=anho)
			
			for fila in filas:
				models.Tarea.objects.filter(fila_planilla=fila.id,nro_planilla=planilla_nro).delete()
				fila.delete()
				
			#borrar archivo subido
			print "Cantidad de archivos subidos ", models.UploadFile.objects.filter(departamento_id=departamento_obj.codigo_jurisdiccion,distrito_id=distrito_obj.codigo_jurisdiccion,nro_planilla=planilla_nro,anho=anho).count()
			models.UploadFile.objects.filter(departamento_id=departamento_obj.codigo_jurisdiccion,distrito_id=distrito_obj.codigo_jurisdiccion,nro_planilla=planilla_nro,anho=anho).delete()
			


		return {"lectura":True, 
		"hoja":hoja,"planilla":planilla_nro, 
		"departamento":departamento_obj, 
		"distrito":distrito_obj,
		"inicio_datos":fila_inicio_datos,
		"fin_datos":fila_fin_datos, 
		"validacion":columnas_func_validacion, 
		"nombre":columnas_nombre, "atributos":columnas_atributos, 
		"libro_para_escritura":wb }
	else:
		return {"lectura":False, "razon":"La planilla que se intenta cargar ya fue entregada previamente."}

def xls_a_bd(planilla_nro,hoja,inicio_datos,fin_datos,columnas_atributos,departamento_obj,distrito_obj,anho):
	for row in range(inicio_datos,fin_datos):
		fila_extraida = {}
		for col in columnas_atributos.keys():
			fila_extraida[columnas_atributos[col]] = transformar(hoja.cell(row,col).value)
		
		#Creamos el objeto correspondiente
		objeto = None
		if planilla_nro == 7:
			#7:cabecera_inmobiliario
			objeto = models.PlanillaInfraestructura()
		if planilla_nro == 9:
			#9:cabecera_aulas,
			objeto = models.PlanillaInfraestructura()
		if planilla_nro == 10:
			objeto = models.PlanillaInfraestructura()
		if planilla_nro == 11:
			#11:cabecera_otros
			objeto = models.PlanillaInfraestructura()
		if planilla_nro == 12:
			#12:cabecera_alimentos
			objeto = models.PlanillaAlimentacion()
		#Se convierte el diccionario en un objeto que representa una tabla en la BD
		models.dic2obj(fila_extraida,objeto)
		objeto.nro_planilla = planilla_nro
		
		objeto.departamento_id = departamento_obj.codigo_jurisdiccion
		objeto.departamento =  departamento_obj.nombre_jurisdiccion

		objeto.distrito_id = distrito_obj.codigo_jurisdiccion
		objeto.distrito = distrito_obj.nombre_jurisdiccion

		objeto.anho = anho
		validadorFondo = validar_fondo.ValidarPlanilla()
		#Se guarda el objeto en la base de datos
		
		objeto.save()
		validadorFondo.validaciones(objeto,planilla_nro)
		#validar forma
		






def validar(archivo,anho,operacion):
	
	#Lectura
	resultado = leer_xls(archivo,anho,operacion)
	if resultado["lectura"]:
		hoja = resultado["hoja"] #Planilla abierta para lectura
		inicio_datos = resultado["inicio_datos"] #Fila donde inician los datos
		fin_datos = resultado["fin_datos"]
		column_type = resultado["validacion"] #
		column_name = resultado["nombre"] #
		columnas_atributos = resultado["atributos"] #  
		libro_salida = resultado["libro_para_escritura"] #Planilla donde se realizaran las marcaciones
		planilla_nro = resultado["planilla"] # Que planilla se leyo
		departamento = resultado["departamento"]
		distrito = resultado["distrito"]
	else:
		#Agregar mas parametros
		return {"valido":False, "marcado":None, "log":False,"errores":"", "messages":[{"tags":"error", "text":resultado["razon"]}]}

	#Validacion
	archivo_valido = True
	errorlog = ""
	#with codecs.open("/tmp/errores.txt","w","utf-8") as archivo:
	
	#Validar el tipo de datos en cada columna
	for col in column_name.keys():
		for row in range(inicio_datos,fin_datos):
			resultado_validacion = column_type[col](hoja.cell(row,col).value)
			if not resultado_validacion["valido"]:
				archivo_valido = False
				errorlog = errorlog + (column_name[col]+" en la celda "+str(EXCEL_COLUMN[col])+str(row+1)+": "+resultado_validacion["motivo"]+" Valor Encontrado: "+str(hoja.cell(row,col).value)+"\n")
				if "correccion" in resultado_validacion.keys():
					libro_salida.get_sheet(0).write(row,col,resultado_validacion["correccion"],resultado_validacion["estilo"])					
				else:
				#archivo.write(errorlog)
				#Se marca en el libro la celda invalida con el color indicado para el error
					libro_salida.get_sheet(0).write(row,col,hoja.cell(row,col).value,resultado_validacion["estilo"])
				#archivo.write("Valor encontrado: "+hoja.cell(row,col).value+" en la fila "+str(row+1)+" en la columna "+str(EXCEL_COLUMN[col])+":"+column_name[col]+"\n")

	archivo_temporal = None
	#Si la planilla no paso alguna validacion, se debe hacer disponible la planilla modificada	
	if not archivo_valido:
		tup = tempfile.mkstemp() # make a tmp file
		f = os.fdopen(tup[0], 'w') # open the tmp file for writing
		libro_salida.save(f)
		f.close()

		### return the path of the file
		archivo_temporal = tup[1] # get the filepath
		log = True
		razon = "La planilla contiene errores de formato. Los mismos son descriptos en la sección \"Errores\". \nTambién puede descargar la planilla con los errores marcados haciendo click en el bóton de Descarga más abajo."
		tag ="error"
	else:
		
		##
		#mudar codigo de verificacion de si ya estaba cargada planilla existencia
		##

		razon = "La planilla ha sido cargada con éxito. Puede verificar en la sección información más datos acerca de la operación."
		tag = "success"
		log = False
		#Si el archivo es valido, podemos volcar en la BD
		xls_a_bd(planilla_nro,hoja,inicio_datos,fin_datos,columnas_atributos,departamento,distrito,anho)
	
	##Obtener distrito
	#for col, nombre in column_name.iteritems():
	#	if fuzz.ratio(nombre,"Distrito") > 70:
	#		columna_distrito = col
	
	#distrito = hoja.cell(inicio_datos,columna_distrito).value

	#Guardar en un stream
	#archivo_marcado = StringIO.StringIO()
	#libro_salida.save(archivo_marcado)
	#libro_salida.save("/tmp/Planilla_modificada.xls")		
		
	return {"valido":archivo_valido, 
			"log":log,
			"errores":errorlog,
			"marcado":archivo_temporal,
			"nroplanilla":planilla_nro,
			"planilla": constantes.planillas[planilla_nro],
			"departamento":departamento,
			"distrito":distrito,
			"messages":[{"tags":tag, "text":razon}]
			}



