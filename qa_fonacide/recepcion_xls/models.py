from django.db import models
from django.contrib.auth.models import User
from simple_history.models import HistoricalRecords
#from recepcion_xls.validacion_forma import constantes
import os
from fuzzywuzzy import fuzz


#Funciones Auxiliares


def obtenerDptoPorNombre(nombre_departamento):
	departamento_seleccionado = None
	ratio = 0
	ratio_anterior = 0
	for dpto in Jurisdiccion.objects.using("mec_public").filter(numero_nivel_jurisdiccion=2):
		ratio = fuzz.ratio(dpto.nombre_jurisdiccion.lower().strip(),nombre_departamento.lower().strip())
		if ratio > ratio_anterior:
			ratio_anterior = ratio
			departamento_seleccionado = dpto
	
	return departamento_seleccionado
	
def obtenerDistritoPorNombre(nombre_distrito,dpto_obj): #Objeto jurisdiccion
	distrito_seleccionado =  None
	ratio = 0
	ratio_anterior = 0
	for dist in Jurisdiccion.objects.using("mec_public").filter(numero_nivel_jurisdiccion=3,id_jurisdiccion_superior=dpto_obj.id_jurisdiccion):
		ratio = fuzz.ratio(dist.nombre_jurisdiccion.lower().strip(),nombre_distrito.lower().strip())
		if ratio > ratio_anterior:
			ratio_anterior = ratio
			distrito_seleccionado = dist
	#Objeto jurisdiccion
	return distrito_seleccionado
		


#diccionario a objeto
def dic2obj(diccionario,objeto):
	

	for atributo in diccionario.keys():
		#setattr(objeto,atributo,valor)
		setattr(objeto,atributo,diccionario[atributo])


	# #Obtenemos los atributos (eliminando los primeros dos atributos doc y module)
	# atributos = dir(objeto)
	# for atributo in atributos:
	# 	#setattr(object, name, value)
	# 	#Seteamos el valor indicado por el diccionario a su correspondiente atributo
	# 	if atributo in diccionario.keys():
	# 		#if atributo == "departamento":
	# 		#	objeto.departamento_id = normalizarDpto(diccionario[atributo])[0]
	# 		#	diccionario[atributo] = normalizarDpto(diccionario[atributo])[1]
	# 		#	print objeto.departamento_id
	# 		#elif atributo == "distrito":
	# 		#	objeto.distrito_id = normalizarDist(diccionario[atributo])[0]
	# 		#	diccionario[atributo] = normalizarDist(diccionario[atributo])[1]
			
	# 		setattr(objeto, atributo, diccionario[atributo])

def set_file_path(planilla, filename):
    return os.path.join('planillas', planilla, filename)



def qaDB_2_MECDB():
	
	pass

def MECDB_2_qaDB():
	pass


class UserProfile(models.Model):
    # This line is required. Links UserProfile to a User model instance.
    user = models.OneToOneField(User)

    # The additional attributes we wish to include.
    distrito = models.CharField(max_length=500,blank=True,null=True)

    # Override the __unicode__() method to return out something meaningful!
    def __unicode__(self):
        return self.user.username


# Create your models here.
class UploadFile(models.Model):
	file = models.FileField(upload_to="planillas")#set_file_path)
	anho = models.IntegerField()
	departamento  = models.CharField(max_length=100)#character varying(100)
	distrito  = models.CharField(max_length=100)#character varying(100),
	departamento_id = models.CharField(max_length=100)
	distrito_id = models.CharField(max_length=100)
	nro_planilla = models.IntegerField() #Not null

class PlanillaInfraestructura(models.Model):
#contenido
	#Comunes (No pueden ser blank o null)
	numero_prioridad  = models.IntegerField(blank=True,null=True)#character varying(100)
	codigo_local  = models.CharField(max_length=100, blank=True,null=True)#character varying(100),
	codigo_institucion  = models.CharField(max_length=100, blank=True,null=True)#character varying(100),
	distancia_este  = models.CharField(max_length=100, blank=True,null=True)#character varying(100),
	distancia_norte  = models.CharField(max_length=100, blank=True,null=True)#character varying(100),
	latitud  = models.CharField(max_length=100, blank=True,null=True)#character varying(100),
	longitud  = models.CharField(max_length=100, blank=True,null=True)#character varying(100),
	nombre_institucion  = models.CharField(max_length=200, blank=True,null=True)#character varying(200),
	distrito  = models.CharField(max_length=100, blank=True,null=True)#character varying(100),
	localidad  = models.CharField(max_length=100, blank=True,null=True)#character varying(100),
	zona  = models.CharField(max_length=100, blank=True,null=True)#character varying(100),
	asentamiento_colonia  = models.CharField(max_length=2000, blank=True,null=True)#character varying(2000),
	nivel_educativo_beneficiado  = models.CharField(max_length=100, blank=True,null=True)#character varying(100),
	abastecimiento_agua  = models.CharField(max_length=100, blank=True,null=True)#character varying(100),
	suministro_energia_electrica  = models.CharField(max_length=100, blank=True,null=True)#character varying(100),
	servicio_sanitario  = models.CharField(max_length=100, blank=True,null=True)#character varying(100),
	titulo_propiedad  = models.CharField(max_length=100, blank=True,null=True)#character varying(100),
	cuenta_con_plano  = models.CharField(max_length=100, blank=True,null=True)#character varying(100),
	justificacion  = models.CharField(max_length=2000, blank=True,null=True)#character varying(2000),			
	cuenta_mecanismo_prevencion_incendio = models.CharField(max_length=100, blank=True,null=True)#character varying(100),
	
	
	#construccion_nueva  = models.IntegerField(blank=True, null=True)
	#construccion_reparar  = models.IntegerField(blank=True, null=True)
	#construccion_adecuar  = models.IntegerField(blank=True, null=True)
 	
 	#Aula|Sanitario|Otros espacios
 	numero_beneficiados = models.IntegerField(blank=True, null=True)
	cuenta_con_espacio_para_construccion = models.CharField(max_length=100,blank=True, null=True)
	
	

	#Solo de Mobiliarios
	silla_cantidad = models.IntegerField(blank=True, null=True)
	sillas_beneficiados = models.IntegerField(blank=True, null=True)
	pizarron_cantidad = models.IntegerField(blank=True, null=True)
	pizarron_beneficiados = models.IntegerField(blank=True, null=True)
	otros_cantidad = models.CharField(blank=True, null=True,max_length=1000)
	otros_beneficiados = models.IntegerField(blank=True, null=True)

	#Solo de Otros espacios #Aula|Sanitario 
	construccion_nueva_cantidad = models.IntegerField(blank=True, null=True)
	construccion_nueva_espacio = models.CharField(max_length=2000,blank=True, null=True)
	construccion_reparar_cantidad = models.IntegerField(blank=True, null=True)
	construccion_reparar_espacio = models.CharField(max_length=2000,blank=True, null=True)
	construccion_adecuar_cantidad = models.IntegerField(blank=True, null=True)
	construccion_adecuar_espacio = models.CharField(max_length=2000,blank=True, null=True)

	#Agregados
	departamento  = models.CharField(max_length=100, blank=True,null=True)#character varying(100)
	departamento_id = models.CharField(max_length=100, blank=True,null=True)
	distrito_id = models.CharField(max_length=100, blank=True,null=True)
#validacion
	#es_cod_inst = models.NullBooleanField()
	#es_cod_local = models.NullBooleanField()
	#esta_inst_local = models.NullBooleanField()

#para control interno
	nro_planilla = models.IntegerField() #Not null
	history = HistoricalRecords()
	anho = models.IntegerField()
	tareas_asociadas = models.IntegerField(default=0)

	#version = models.IntegerField(null=True)
	#id_planilla = models.AutoField(primary_key=True)

	def __unicode__(self):
		return "Planilla Nro. " +str(self.nro_planilla)+" "+self.departamento+"-"+self.distrito+" Prioridad: "+self.numero_prioridad
	
class PlanillaAlimentacion(models.Model):
	numero_prioridad  = models.IntegerField(blank=True,null=True)#character varying(100)
	codigo_local  = models.CharField(max_length=100, blank=True,null=True)#character varying(100),
	codigo_institucion  = models.CharField(max_length=100, blank=True,null=True)#character varying(100),
	distancia_este  = models.CharField(max_length=100, blank=True,null=True)#character varying(100),
	distancia_norte  = models.CharField(max_length=100, blank=True,null=True)#character varying(100),
	latitud  = models.CharField(max_length=100, blank=True,null=True)#character varying(100),
	longitud  = models.CharField(max_length=100, blank=True,null=True)#character varying(100),
	nombre_institucion  = models.CharField(max_length=200, blank=True,null=True)#character varying(200),
	localidad  = models.CharField(max_length=100, blank=True,null=True)#character varying(100),
	zona  = models.CharField(max_length=100, blank=True,null=True)#character varying(100),
	asentamiento_colonia  = models.CharField(max_length=2000, blank=True,null=True)#character varying(2000),
	numero_beneficiados_m = models.IntegerField(default=0, blank=True,null=True)
	numero_beneficiados_f = models.IntegerField(default=0, blank=True,null=True)
	abastecimiento_agua  = models.CharField(max_length=100, blank=True,null=True)#character varying(100),
	suministro_energia_electrica  = models.CharField(max_length=100, blank=True,null=True)#character varying(100),
	servicio_sanitario  = models.CharField(max_length=100, blank=True,null=True)#character varying(100),
	cuenta_con_espacio_cocina_comedor  = models.CharField(max_length=200, blank=True,null=True)
	recibe_merienda_si_leche  = models.CharField(max_length=100, blank=True,null=True)
	recibe_merienda_si_alimento  = models.CharField(max_length=100, blank=True,null=True)
	recibe_merienda_no  = models.CharField(max_length=100, blank=True,null=True)
	recibe_almuerzo_si_fresco_catering   = models.CharField(max_length=100, blank=True,null=True)
	recibe_almuerzo_si_fresco_preparado  = models.CharField(max_length=100, blank=True,null=True)
	recibe_almuerzo_si_fresco_otro  = models.CharField(max_length=100, blank=True,null=True)
	recibe_almuerzo_si_distribucion_catering  = models.CharField(max_length=100, blank=True,null=True)
	recibe_almuerzo_si_distribucion_preparado  = models.CharField(max_length=100, blank=True,null=True)
	recibe_almuerzo_si_distribucion_otros  = models.CharField(max_length=100, blank=True,null=True)
	recibe_almuerzo_no  = models.CharField(max_length=100, blank=True,null=True)
	cantidad_meses_recibio_almuerzo = models.IntegerField(default=0)
	ausentismo_0_10  = models.CharField(max_length=100, blank=True,null=True)
	ausentismo_11_25  = models.CharField(max_length=100, blank=True,null=True)
	ausentismo_25  = models.CharField(max_length=100, blank=True,null=True)
	escuela_doble_escolaridad  = models.CharField(max_length=100, blank=True,null=True)
	justificacion  = models.CharField(max_length=2000, blank=True,null=True)#character varying(2000),			
	departamento = models.CharField(max_length=100, blank=True,null=True)
	distrito = models.CharField(max_length=100, blank=True,null=True)
	
	departamento_id = models.CharField(max_length=100, blank=True,null=True)
	distrito_id = models.CharField(max_length=100, blank=True,null=True)

	#Para control interno
	nro_planilla = models.IntegerField(default=12)
	history = HistoricalRecords()
	anho = models.IntegerField()
	tareas_asociadas = models.IntegerField(default=0)
	

	def __unicode__(self):
		return "Planilla Nro. " +str(self.nro_planilla)+" "+self.departamento+"-"+self.distrito+" Prioridad: "+self.numero_prioridad


class Tarea(models.Model):
	
	id_tarea = models.AutoField(primary_key=True)
	fila_planilla = models.IntegerField()
	nro_planilla = models.IntegerField()
	tipo  = models.CharField(max_length=100)
	version_actual = models.IntegerField(blank=True,null=True) #Version usada en la creacion de la tarea
	version_modificada = models.IntegerField(blank=True,null=True) #Version obtenida al realizar la tarea
	estado = models.CharField(max_length=30)
	usuario = models.ForeignKey(User,blank=True, null=True)
	fecha = models.DateTimeField()
	


class Jurisdiccion(models.Model):
	class Meta:
		managed = True
		db_table = 'jurisdiccion'


	id_jurisdiccion = models.BigIntegerField(primary_key=True) #bigint NOT NULL,
	version_jurisdiccion = models.BigIntegerField(default=0)#bigint NOT NULL DEFAULT 0,
	codigo_jurisdiccion = models.CharField(max_length=30)#character varying(30) NOT NULL,
	nombre_jurisdiccion =models.CharField(max_length=100)#character varying(100) NOT NULL,
	id_jurisdiccion_superior = models.BigIntegerField()#bigint,
	numero_tipo_nodo = models.IntegerField()#integer,
	numero_nivel_jurisdiccion = models.IntegerField()#integer,
	secuencia_jurisdiccion = models.IntegerField()#integer,
	clave_jurisdiccion = models.CharField(max_length=30)#character varying(30),
	id_jurisdiccion_real = models.BigIntegerField() #bigint, -- Este campo sera diferente a id_jurisdiccion cuando haya diferencias entre las jurisdicciones cartograficas censales y distritales

	def __unicode__(self):
		return self.nombre_jurisdiccion


###Tablas MECDB100 (Solo lectura)
#No usar
#class InstitucionSIEC(models.Model):
#	class Meta:
#		managed = False
#		db_table = 'institucion_siec_datos_abiertos'

#	periodo = models.IntegerField()#integer,
#	codigo_departamento  = models.CharField(max_length=100)#character varying(100),
#	nombre_departamento = models.CharField(max_length=100) #character varying(100),
#	codigo_distrito = models.CharField(max_length=100) #character varying(100),
#	nombre_distrito = models.CharField(max_length=100) #character varying(100),
#	codigo_barrio_localidad = models.CharField(max_length=100) #character varying(100),
#	nombre_barrio_localidad = models.CharField(max_length=100) #character varying(100),
#	codigo_zona = models.CharField(max_length=100) #character varying(100),
#	nombre_zona = models.CharField(max_length=100) #character varying(100),
#	codigo_establecimiento = models.CharField(max_length=100,primary_key=True) #character varying(100) NOT NULL,
#	codigo_institucion = models.CharField(max_length=100,primary_key=True) #character varying(100) NOT NULL,
#	nombre_institucion = models.CharField(max_length=100) #character varying(500),
#	anho_cod_geo = models.IntegerField() #integer,
#	uri_establecimiento = models.CharField(max_length=100) #character varying(100),
#	uri_institucion = models.CharField(max_length=100) #character varying(100),
  	#CONSTRAINT institucion_siec_pkey PRIMARY KEY (codigo_establecimiento , codigo_institucion ))


class Codiniv2014(models.Model):
 	class Meta:
 		managed = True
 		db_table = 'codiniv2014' 
 	dpto = models.CharField(max_length=100)
 	dpto_desc = models.CharField(max_length=100)
 	distrito = models.CharField(max_length=100)
 	dist_desc = models.CharField(max_length=100)
 	bar_loc = models.CharField(max_length=100)
 	barlo_desc = models.CharField(max_length=100)
 	area = models.CharField(max_length=100)
 	codigo_establecimiento = models.CharField(max_length=100)
 	codigo_institucion = models.CharField(max_length=100)
 	codigo_oferta_educativa = models.CharField(max_length=100, primary_key=True)
 	nivid = models.CharField(max_length=100)
 	tpoinstdsc = models.CharField(max_length=100)
 	nombre = models.CharField(max_length=100)
 	tponivcod = models.CharField(max_length=100)
 	listado_codiniv_2014_direccion = models.CharField(max_length=100)
 	telefono = models.CharField(max_length=100)
 	ofertapaginaweb = models.CharField(max_length=100)
 	ofertacorreoelectronico = models.CharField(max_length=100)
 	codigo_region_administrativa = models.CharField(max_length=100)
 	nombre_region_administrativa = models.CharField(max_length=100)
 	sector_o_t = models.CharField(max_length=100)
 	estado_de_funcionamiento_2013 = models.CharField(max_length=100)
 

#Modelos de mec_public

# class Institucion
class Institucion(models.Model):
	class Meta:
		managed = False
		db_table = 'institucion'

	id_institucion = models.IntegerField(primary_key=True) #bigint NOT NULL,
	version_institucion = models.IntegerField() #bigint NOT NULL DEFAULT 0,
	codigo_institucion = models.CharField(max_length=30)#character varying(30) NOT NULL,
	nombre_institucion = models.CharField(max_length=100)#character varying(100) NOT NULL,
	codigo_institucion_planificacion = models.CharField(max_length=30) #character varying(30),
	direccion_institucion = models.CharField(max_length=2000) #character varying(2000),
	id_jurisdiccion_direccion_institucion = models.IntegerField()#bigint NOT NULL,
	telefono_institucion = models.CharField(max_length=100)#character varying(100),
	fax_institucion = models.CharField(max_length=100)#character varying(100),
	correo_electronico_institucion = models.CharField(max_length=100) #character varying(100),
	id_matriz = models.IntegerField()#bigint NOT NULL,
	id_dependencia = models.IntegerField(max_length=30) #bigint,
	numero_tipo_institucion = models.IntegerField() #integer NOT NULL,
	numero_tipo_administracion = models.IntegerField() #integer NOT NULL DEFAULT 1,
	numero_tipo_gestion = models.IntegerField() #integer NOT NULL DEFAULT 1,
	numero_tipo_zona = models.IntegerField()#integer NOT NULL DEFAULT 1,
	numero_area_educativa = models.IntegerField() #integer,
	es_institucion_indigena = models.IntegerField() #integer NOT NULL DEFAULT 0,
	es_institucion_intervenida = models.IntegerField()#integer NOT NULL DEFAULT 0,
	es_institucion_inactiva = models.IntegerField()#integer NOT NULL DEFAULT 0,
	codigo_establecimiento_escolar = models.CharField(max_length=30)#character varying(30),
	codigo_nivel_modalidad = models.CharField(max_length=30)#character varying(30),
	resolucion_apertura_institucion = models.CharField(max_length=10)#character varying(10),
	fecha_apertura_institucion  = models.DateTimeField() #without time zone,


class Local(models.Model):
	class Meta:
		managed = False
		db_table = 'local_escolar'

	id_local_escolar = models.IntegerField(primary_key=True) #bigint NOT NULL,
	version_local_escolar = models.IntegerField()#bigint NOT NULL DEFAULT 0,
	codigo_local_escolar = models.CharField(max_length=30)#character varying(30) NOT NULL,
	nombre_local_escolar = models.CharField(max_length=100) #character varying(100),
	direccion_local_escolar = models.CharField(max_length=2000)#character varying(2000),
	nombre_calle_principal = models.CharField(max_length=30) #character varying(30),
	nombre_calle_interseccion = models.CharField(max_length=30) #character varying(30),
	nombre_edificio = models.CharField(max_length=30) #character varying(30),
	numero_edificio = models.CharField(max_length=10) #character varying(10),
	numero_piso = models.CharField(max_length=10) #character varying(10),
	numero_oficina = models.CharField(max_length=10)#character varying(10),
	id_jurisdiccion_direccion_local_escolar = models.IntegerField() #bigint NOT NULL,
	telefono_local_escolar = models.CharField(max_length=100) #character varying(100),
	fax_local_escolar = models.CharField(max_length=100) #character varying(100),
	correo_electronico_local_escolar = models.CharField(max_length=100) #character varying(100),
	url_pagina_web_local_escolar = models.CharField(max_length=2000) #character varying(2000),
	planilla_local_escolar = models.CharField(max_length=30) #character varying(30),
	numero_tipo_zona = models.IntegerField() #integer,
	contador_gps = models.CharField(max_length=30) #character varying(30),
	huso_utm = models.IntegerField() #integer,
	franja_utm = models.CharField(max_length=1) #character varying(1),
	distancia_este_utm = models.IntegerField() #integer,
	distancia_norte_utm = models.IntegerField() #integer,
	latitud_local_escolar = models.CharField(max_length=10) #character varying(10),
	longitud_local_escolar = models.CharField(max_length=10) #character varying(10),
	es_local_con_internet = models.IntegerField() #integer NOT NULL DEFAULT 0,
	es_local_con_telefono = models.IntegerField() #integer NOT NULL DEFAULT 0,
	area_terreno = models.IntegerField() #integer,
	area_construccion = models.IntegerField() #integer,
	area_recreacion = models.IntegerField() #integer,
	cantidad_bloques = models.IntegerField() #integer,
	cantidad_plantas = models.IntegerField() #integer,
	cantidad_aulas_otro_fin = models.IntegerField() #integer,
	cantidad_aulas_faltantes = models.IntegerField()#integer,
	es_local_con_espacio_aulas_nuevas = models.IntegerField()#integer NOT NULL DEFAULT 0,
	cantidad_sanitarios_varones = models.IntegerField()#integer,
	cantidad_sanitarios_hembras = models.IntegerField()#integer,
	es_local_con_sanitario_preescolar = models.IntegerField()#integer NOT NULL DEFAULT 0,
	numero_tipo_abastecimiento_agua = models.IntegerField()#integer,
	numero_tipo_cercado = models.IntegerField()#integer,
	numero_tipo_fuente_energia = models.IntegerField()#integer,
	numero_tipo_propiedad_terreno = models.IntegerField()#integer,
	numero_tipo_servicio_sanitario = models.IntegerField()#integer,
	numero_tipo_titulo_propiedad = models.IntegerField()#integer,
	numero_tipo_via_acceso = models.IntegerField()#integer,
	otro_tipo_abastecimiento_agua = models.CharField(max_length=100)#character varying(100),
	otro_tipo_cercado = models.CharField(max_length=100)#character varying(100),
	otro_tipo_fuente_energia = models.CharField(max_length=100)#character varying(100),
	otro_tipo_propiedad_terreno = models.CharField(max_length=100)#character varying(100),
	otro_tipo_servicio_sanitario = models.CharField(max_length=100)#character varying(100),
	otro_tipo_titulo_propiedad = models.CharField(max_length=100)#character varying(100),
	otro_tipo_via_acceso = models.CharField(max_length=100)#character varying(100),
	cuenta_con_espacio_para_construccion = models.BooleanField(default=False)#boolean NOT NULL DEFAULT false,
	cuenta_con_mecanismo_prevencion_incendio = models.BooleanField(default=False)#boolean NOT NULL DEFAULT false,
	cuenta_con_plano  = models.BooleanField(default=False)#boolean NOT NULL DEFAULT false,
	nombre_asentamiento_colonia = models.CharField(max_length=50)#character varying(50),




class PlanillaAlimentacionEscolarMECDB(models.Model):
	class Meta:
		managed = False
		db_table = 'planilla_alimentacion_escolar'


###Para alimentacion escolar 

# -- Table: planilla_alimentacion_escolar

# -- DROP TABLE planilla_alimentacion_escolar;

# CREATE TABLE planilla_alimentacion_escolar
# (
 #  id_planilla = models.IntegerField() #integer NOT NULL,
  # departamento = models.CharField(max_length=100) #character varying(100) NOT NULL,
   #distrito character varying(100) NOT NULL,
   #numero_prioridad character varying(100) NOT NULL,
#   codigo_local character varying(100),
#   codigo_institucion character varying(100),
#   este character varying(100),
#   norte character varying(100),
#   latitud character varying(100),
#   longitud character varying(100),
#   nombre_institucion character varying(100),
#   localidad_barrio character varying(100),
#   zona character varying(100),
#   nombre_asentamiento_comunidad_indigena character varying(200),
#   numero_matriculados_masculinos character varying(100),
#   numero_matriculados_femeninos character varying(100),
#   abastecimiento_agua character varying(100),
#   suministro_energia_electrica character varying(100),
#   servicio_sanitario character varying(100),
#   cuenta_espacio_construccion_cocina_comedor character varying(100),
#   merienda_leche character varying(100),
#   merienda_alimento_solido character varying(100),
#   merienda_no character varying(100),
#   almuerzo_fresco_catering character varying(100),
#   almuerzo_fresco_preparado_local character varying(500),
#   almuerzo_fresco_otro character varying(100),
#   almuerzo_no character varying(100),
#   almuerzo_meses character varying(100),
#   porcentaje_000_010 character varying(100),
#   porcentaje_011_025 character varying(100),
#   porcentaje_026_100 character varying(100),
#   doble_escolaridad character varying(100),
#   justificacion character varying(2000),
#   migrado_siec boolean,
#   esta_local_codiniv boolean,
#   esta_institucion_codiniv boolean,
#   esta_institucion_en_local boolean,
#   id_departamento bigint,
#   id_distrito bigint,
#   fecha_migracion timestamp without time zone,
#   observacion_migracion character varying(2000),
#   merienda_leche_normalizado character varying(100),
#   merienda_no_normalizado character varying(10),
#   merienda_alimento_solido_normalizado character varying(100),
#   almuerzo_no_normalizado character varying(50),
#   numero_matriculados_masculinos_normalizado character varying(100) NOT NULL,
#   numero_matriculados_femeninos_normalizado character varying(100) NOT NULL,
#   cuenta_espacio_construccion_cocina_comedor_normalizado character varying(100),
#   porcentaje_000_010_normalizado character varying(100),
#   porcentaje_011_025_normalizado character varying(100),
#   porcentaje_026_100_normalizado character varying(100),
#   doble_escolaridad_normalizado character varying(50),
#   almuerzo_deshidratado_catering character varying(100),
#   almuerzo_deshidratado_preparado_local character varying(100),
#   almuerzo_deshidratado_otro character varying(100),
#   almuerzo_fresco_catering_normalizado character varying(200),
#   almuerzo_fresco_preparado_local_normalizado character varying(200),
#   almuerzo_fresco_otro_normalizado character varying(200),
#   almuerzo_deshidratado_catering_normalizado character varying(200),
#   almuerzo_deshidratado_preparado_local_normalizado character varying(200),
#   almuerzo_deshidratado_otro_normalizado character varying(200),
#   observacion_depuracion_datos character varying(2000),
#   numero_prioridad_normalizado character varying(100) NOT NULL,
#   codigo_local_normalizado character varying(100),
#   codigo_institucion_normalizado character varying(100),
#   almuerzo_meses_normalizado character varying(100),
#   porcentaje_similaridad_nombre_institucion integer,
#   CONSTRAINT planilla_alimentacion_escolar_pkey PRIMARY KEY (id_planilla , departamento , distrito , numero_prioridad_normalizado )
# )


#Revisar la tabla infraestructura
# -- Table: planilla_infraestructura

# -- DROP TABLE planilla_infraestructura;

# CREATE TABLE planilla_infraestructura
# (
#   numero_fila_excel integer NOT NULL,
#   departamento character varying(20),
#   nombre_espacio character varying(20),
#   numero_prioridad character varying(100),
#   codigo_local character varying(100),
#   codigo_institucion character varying(100),
#   distancia_este character varying(100),
#   distancia_norte character varying(100),
#   latitud character varying(100),
#   longitud character varying(100),
#   nombre_institucion character varying(200),
#   distrito character varying(100),
#   localidad character varying(100),
#   zona character varying(100),
#   asentamiento_colonia character varying(200),
#   numero_beneficiados character varying(100),
#   nivel_educativo_beneficiado character varying(100),
#   espacio_nueva character varying(200),
#   cantidad_nueva character varying(200),
#   espacio_reparar character varying(200),
#   cantidad_reparar character varying(200),
#   espacio_adecuar character varying(200),
#   cantidad_adecuar character varying(200),
#   abastecimiento_agua character varying(100),
#   suministro_energia_electrica character varying(100),
#   servicio_sanitario character varying(100),
#   titulo_propiedad character varying(100),
#   cuenta_con_plano character varying(100),
#   cuenta_espacio_construccion character varying(100),
#   cuenta_mecanismo_prevencion_incendio character varying(100),
#   justificacion character varying(2000),
#   migrado boolean,
#   esta_local_tabla_sigmec boolean,
#   esta_local_codiniv boolean,
#   esta_institucion_sigmec boolean,
#   esta_institucion_codiniv boolean,
#   observacion_institucion_sigmec character varying(100),
#   esta_institucion_en_local boolean,
#   id_departamento bigint,
#   id_distrito bigint,
#   fecha_migracion timestamp without time zone,
#   observacion_migracion character varying(2000),
#   es_numero_prioridad_valido boolean,
#   es_cantidad_nueva_valida boolean,
#   es_cantidad_reparar_valida boolean,
#   es_cantidad_adecuar_valida boolean,
#   es_numero_beneficiados_valido boolean,
#   espacio_nuevo_normalizado character varying(500),
#   cantidad_nuevo_normalizado character varying(100),
#   estacio_reparar_normalizado character varying(100),
#   cantidad_reparar_normalizado character varying(100),
#   espacio_adecuar_normalizado character varying(100),
#   cantidad_adecuar_normalizado character varying(100),
#   numero_beneficiados_normalizado character varying(100),
#   codigo_institucion_sigmec character varying(20),
#   departamento2 character varying(100),
#   distrito2 character varying(100),
#   id_departamento2 bigint,
#   id_distrito2 bigint,
#   migrado_siec boolean,
#   estado_correccion character varying(20),
#   usuario_responsable_correccion character varying(50),
#   CONSTRAINT planilla_infraestructura_pkey PRIMARY KEY (numero_fila_excel )
# )


#Para Mobiliarios
#-- Table: planilla_mobiliario

# -- DROP TABLE planilla_mobiliario;

# CREATE TABLE planilla_mobiliario
# (
#   departamento character varying(100),
#   numero_prioridad character varying(100),
#   codigo_local character varying(100),
#   codigo_institucion character varying(100),
#   distancia_este character varying(100),
#   distancia_norte character varying(100),
#   latitud character varying(100),
#   longitud character varying(100),
#   nombre_institucion character varying(200),
#   distrito character varying(100),
#   localidad character varying(100),
#   zona character varying(100),
#   asentamiento_colonia character varying(2000),
#   abastecimiento_agua character varying(100),
#   suministro_energia_electrica character varying(100),
#   servicio_sanitario character varying(100),
#   titulo_propiedad character varying(100),
#   cuenta_con_plano character varying(100),
#   cuenta_mecanismo_prevencion_incendio character varying(100),
#   nivel_educativo_beneficiado character varying(100),
#   silla_cantidad character varying(100),
#   silla_numero_beneficiados character varying(100),
#   pizarron_cantidad character varying(100),
#   pizarron_numero_beneficiados character varying(100),
#   otro_nombre_cantidad character varying(500),
#   otro_numero_beneficiados character varying(100),
#   justificacion character varying(2000),
#   id_planilla integer NOT NULL,
#   otro_nombre_cantidad_normalizado character varying(500),
#   migrado boolean DEFAULT false,
#   fecha_migracion timestamp without time zone,
#   observacion_migracion character varying(2000),
#   id_departamento bigint,
#   id_distrito bigint,
#   esta_local_tabla_sigmec boolean,
#   esta_local_codiniv boolean,
#   esta_institucion_sigmec boolean,
#   observacion_institucion_sigmec character varying(5),
#   esta_institucion_codiniv boolean,
#   esta_institucion_en_local boolean,
#   id_requerimiento bigint,
#   departamento2 character varying(100),
#   distrito2 character varying(100),
#   id_departamento2 bigint,
#   id_distrito2 bigint,
#   codigo_institucion_sigmec character varying(100),
#   migrado_siec boolean,
#   CONSTRAINT planilla_mobiliario_pkey PRIMARY KEY (id_planilla )
# )
# WITH (
#   OIDS=FALSE
# );
# ALTER TABLE planilla_mobiliario
#   OWNER TO postgres;
# GRANT ALL ON TABLE planilla_mobiliario TO postgres;
# GRANT SELECT ON TABLE planilla_mobiliario TO opendata;