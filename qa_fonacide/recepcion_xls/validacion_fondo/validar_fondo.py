#coding: UTF-8
from abc import ABCMeta, abstractmethod
import validadores
import tareas



class ValidarFondo:
	__metaclass__ = ABCMeta

	
	validadores = {}
	correccion_automatica = {}
	tareas = {}

	def validaciones(self,fila_planilla,nro_planilla):
		"""
		recibe una lista de validadores con el siguiente formato:
		{1:[validar_inst(fila_planilla),validar_local(fila_planilla)],2:[validar_local_inst(fila_planilla)]

		En un mismo nivel se encuentran los validadores que son independientes entre si
		sea porque no validan el mismo campo o validan diferentes aspectos de un mismo campo.
		Validadores en un siguiente nivel dependen de que los validadores de un nivel anterior hayan terminado correctamente
		Si en un nivel un validador falla, se terminan los validadores restantes del nivel pero no se pasa a los siguientes niveles

		"""
		#Se empieza en el primer nivel de validacion
		nivel = 0
		maxnivel = max(self.validadores.keys())
		seguir_validando = True
		while seguir_validando and nivel < maxnivel:
			nivel = nivel + 1
			validaciones_actuales = self.validadores[nivel]
			correctores_actuales = self.correccion_automatica[nivel]
			tareas_actuales = self.tareas[nivel]
			for i in range(len(validaciones_actuales)):
				validador = validaciones_actuales[i]
				corrector = correctores_actuales[i]
				tarea = tareas_actuales[i]
				
				resultado_validacion = validador(fila_planilla,nro_planilla)
				if resultado_validacion == False:
					
					#se crea la tarea
					if not False:#corrector(fila_planilla,nro_planilla):
						tarea(fila_planilla,nro_planilla)
						seguir_validando = False #Evitamos que avance al siguiente nivel de las validaciones




#Recibir señal de creacion/modificacion de una planilla Sanitario

class ValidarPlanilla(ValidarFondo):
	validadores = {	1:[	validadores.validar_codigos],
				}
	correccion_automatica = {1:[tareas.corregir_auto_codigos]}

	tareas = {	1:[	tareas.crear_tarea_corregir_codigos,
					],
			}


