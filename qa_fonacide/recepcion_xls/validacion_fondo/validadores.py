#coding: UTF-8
#Acceso a la BD
from recepcion_xls import models 
#Comparacion difusa
from fuzzywuzzy import fuzz


def es_cod_institucion_valido(fila_planilla,nro_planilla):
	
	"""
	Recibe el codigo de institucion y el nombre de la institucion
	Se chequea que el codigo exista en la tabla SIEC.
	Si existe se compara el nombre en la tabla con el recibido como parametro (comparación difusa)

	"""

	cod_inst = fila_planilla.codigo_institucion
	nombre = fila_planilla.nombre_institucion

	instCodiniv = models.Codiniv2014.objects.using("mec_public").filter(codigo_institucion=cod_inst)
	if len(instCodiniv) > 0:
		return True
	else:
		return False
	#if len(instCodiniv) > 0:
	#	#Se toma el primero
	#	instCodiniv = instCodiniv[0]
	#	ratio = fuzz.ratio(instCodiniv.nombre.lower(),nombre.lower())
	#	if ratio > 0:
	#		return True
	#	else:
	#		return False
	#else:
	#	return False


def es_cod_local_valido(fila_planilla,nro_planilla):
	"""
	Recibe el codigo de local y comprueba si existe en la tabla SIEC
	"""

	cod_local = fila_planilla.codigo_local
	
	instCodiniv = models.Codiniv2014.objects.using("mec_public").filter(codigo_establecimiento=cod_local)
	if len(instCodiniv) > 0:
		return True
	else:
		return False

def cod_inst_en_local(fila_planilla,nro_planilla):
	"""
	Recibe un codigo de institucion y un cod de local. Comprueba si 
	el codigo de institucion esta asociado con el cod de local
	"""

	cod_inst = fila_planilla.codigo_institucion
	cod_local = fila_planilla.codigo_local

	instCodiniv = models.Codiniv2014.objects.using("mec_public").filter(codigo_establecimiento=cod_local,codigo_institucion=cod_inst)
	if len(instCodiniv) > 0:
		return True
	else:
		return False

def validar_codigos(fila_planilla,nro_planilla):
	cod_inst = fila_planilla.codigo_institucion
	cod_local = fila_planilla.codigo_local
	return es_cod_institucion_valido(fila_planilla,nro_planilla) and es_cod_local_valido(fila_planilla,nro_planilla) and cod_inst_en_local(fila_planilla,nro_planilla)