#coding: UTF-8
from recepcion_xls import models
from recepcion_xls import constantes
from django.utils import timezone
from fuzzywuzzy import fuzz

#Utilidades

def comparar_sugerencias():
	pass

def fuzzy_list_comp(palabra,objetos,atributo):
	ratio_min = 70
	matched_list = []
	for objeto in objetos:
		ratio = fuzzy.ratio(palabra,getattr(objeto,atributo))
		if  ratio >= ratio_min:
			matched_list.append(objeto)
	return objeto

def get_lista_sugerencias(fila_planilla,nro_planilla):
	try:
		distrito = fila_planilla.distrito
		departamento = fila_planilla.departamento
		nombre = fila_planilla.nombre_institucion
		
		dpto_id = 0
		dist_id = 0
		ratio_dpto = 70
		ratio_dist = 70
		##Buscar
		for i in range(len(constantes.departamentos_distritos)):
			ratio_act_dpto = fuzz.ratio(departamento.lower(),constantes.departamentos_distritos[i][1].lower())
			ratio_act_dist = fuzz.ratio(distrito.lower(),constantes.departamentos_distritos[i][3].lower())
			if ratio_act_dpto > ratio_dpto and ratio_act_dist > ratio_dist:
				dpto_id = constantes.departamentos_distritos[i][0]
				dist_id = constantes.departamentos_distritos[i][2]
				ratio_dpto = ratio_act_dpto
				ratio_dist = ratio_act_dist


		sugerencias = models.Codiniv2014.objects.using("mec_public").filter(distrito=dist_id,dpto=dpto_id).order_by("codigo_institucion","codigo_establecimiento").distinct("codigo_institucion","codigo_establecimiento")
		iguales = []
		mayores = []
		menores = []
		for sugerencia in sugerencias:
			if sugerencia.codigo_establecimiento == fila_planilla.codigo_local or sugerencia.codigo_institucion == fila_planilla.codigo_institucion:
				iguales.append(sugerencia)
			elif sugerencia.codigo_establecimiento < fila_planilla.codigo_local or sugerencia.codigo_institucion < fila_planilla.codigo_institucion: 
				menores.append(sugerencia)
			else:
				mayores.append(sugerencia)

		sugerencias = iguales + menores + mayores

		#sorted(sugerencias, cmp=comparar_sugerencias)
		#sorted(x, cmp=greater)
		#lista_sugerencias = []
		#for sugerencia in sugerencias:
		#	if fuzz.ratio(nombre.lower(),sugerencia.nombre.lower()) > 50:
		#		lista_sugerencias.append(sugerencia)
	
		#Falta filtrar por nombre
		return sugerencias
	except:
		import pdb;pdb.set_trace()

#Tareas

def crear_tarea_corregir_cod_inst(fila_planilla,nro_planilla):
	"""
	Crea una tarea de correccion de codigo_institucion
	"""
	tarea = models.Tarea()
	tarea.fila_planilla_sanitario = fila_planilla
	tarea.tipo = "Corrección de Cod. Institución"
	tarea.valor_actual = fila_planilla.codigo_institucion
	tarea.estado = "No asignada"
	tarea.save()


def crear_tarea_corregir_cod_local(fila_planilla,nro_planilla):
	"""
	Crea una tarea de correccion de local
	"""
	tarea = models.Tarea()
	tarea.fila_planilla_sanitario = fila_planilla
	tarea.tipo = "Corrección de Cod. Local"
	tarea.valor_actual = fila_planilla.codigo_local
	tarea.estado = "No asignada"
	tarea.save()

def crear_tarea_corregir_local_inst(fila_planilla,nro_planilla):
	crear_tarea_corregir_cod_inst(fila_planilla)
	crear_tarea_corregir_cod_local(fila_planilla)


def crear_tarea_corregir_codigos(fila_planilla,nro_planilla):
	tarea = models.Tarea()
	tarea.fila_planilla = fila_planilla.id
	tarea.nro_planilla = nro_planilla
	tarea.tipo = "Corrección de Códigos"
	#if nro_planilla in [7,9,10,11]:
	#	models.PlanillaInfraestructura.objects.using("default").filter(id=)
	#elif nro_planilla == 12:

	#Trae la version mas reciente del historial
	fila_planilla.tareas_asociadas = fila_planilla.tareas_asociadas + 1
	fila_planilla.save()
	tarea.version_actual = fila_planilla.history.all()[0].history_id
	tarea.estado = "No asignada"
	tarea.fecha = timezone.now()
	tarea.save()


def corregir_auto_codigos(fila_planilla,nro_planilla):
	sugerencias = get_lista_sugerencias(fila_planilla,nro_planilla)
	if len(sugerencias) == 1:
		sugerencia = sugerencias[0]
		fila_planilla.codigo_institucion = sugerencia.codigo_institucion
		fila_planilla.codigo_local = sugerencia.codigo_establecimiento
		#fila_planilla.es_cod_local
		#fila_planilla.es_cod_inst
		#fila_planilla.cod_inst_local
		fila_planilla.save()
		return True
	else:
		return False
	