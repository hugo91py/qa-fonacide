#coding: UTF-8
import tempfile
import os
from StringIO import StringIO  
from zipfile import ZipFile  

from numbers import Number

from recaptcha.client import captcha 

from django.contrib.auth import authenticate, login,logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.shortcuts import render, render_to_response, redirect
from django.template import RequestContext
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.core.files import File #Para manejo de archivo
from django.views.decorators.csrf import csrf_exempt



from forms import UserForm,UploadFileForm, CorreccionCodigosForm, UserReCaptchaForm

from models import UploadFile

from recepcion_xls import models

from recepcion_xls.validacion_forma import validar_forma, validadores

import constantes


from registration.backends.simple.views import RegistrationView

from datetime import date,datetime
from django.utils import timezone
from recepcion_xls.validacion_fondo import tareas
from recepcion_xls.validacion_fondo import validadores as validadores_fondo

from qa_fonacide.settings import BASE_DIR





# Create a new class that redirects the user to the index page, if successful at logging
class RedirectRegistrationView(RegistrationView):

	form_class = UserReCaptchaForm
	
	def get_success_url(self,request, user):
		return '/'



def register(request):

	# A boolean value for telling the template whether the registration was successful.
	# Set to False initially. Code changes value to True when registration succeeds.
	registered = False

	# If it's a HTTP POST, we're interested in processing form data.
	if request.method == 'POST':
		# Attempt to grab information from the raw form information.
		# Note that we make use of both UserForm and UserProfileForm.
		user_form = UserForm(data=request.POST)

		# If the two forms are valid...
		if user_form.is_valid():
			# Save the user's form data to the database.
			user = user_form.save()

			# Now we hash the password with the set_password method.
			# Once hashed, we can update the user object.
			user.set_password(user.password)
			user.save()

			# Update our variable to tell the template registration was successful.
			registered = True

		# Invalid form or forms - mistakes or something else?
		# Print problems to the terminal.
		# They'll also be shown to the user.
		else:
			print user_form.errors

	# Not a HTTP POST, so we render our form using two ModelForm instances.
	# These forms will be blank, ready for user input.
	else:
		user_form = UserForm()
	# Render the template depending on the context.
	return render(request,
			'recepcion_xls/register.html',
			{'user_form': user_form, 'registered': registered} )


def user_login(request):

	# If the request is a HTTP POST, try to pull out the relevant information.
	if request.method == 'POST':
		# Gather the username and password provided by the user.
		# This information is obtained from the login form.
		username = request.POST['username']
		password = request.POST['password']

		# Use Django's machinery to attempt to see if the username/password
		# combination is valid - a User object is returned if it is.
		user = authenticate(username=username, password=password)

		# If we have a User object, the details are correct.
		# If None (Python's way of representing the absence of a value), no user
		# with matching credentials was found.
		if user:
			# Is the account active? It could have been disabled.
			if user.is_active:
				# If the account is valid and active, we can log the user in.
				# We'll send the user back to the homepage.
				login(request, user)
				return HttpResponseRedirect('/')
			else:
				# An inactive account was used - no logging in!
				return HttpResponse("Su cuenta ha sido deshabilitada.")
		else:
			# Bad login details were provided. So we can't log the user in.
			print "Usuario y Contraseña invalidos: {0}, {1}".format(username, password)
			return HttpResponse("Invalid login details supplied.")

	# The request is not a HTTP POST, so display the login form.
	# This scenario would most likely be a HTTP GET.
	else:
		# No context variables to pass to the template system, hence the
		# blank dictionary object...
		return render(request, 'recepcion_xls/login.html', {})



# Use the login_required() decorator to ensure only those logged in can access the view.
@login_required
def user_logout(request):
	# Since we know the user is logged in, we can now just log them out.
	logout(request)
	# Take the user back to the homepage.
	return HttpResponseRedirect('/login/')


@login_required
def index(request):
	return render(request, 'recepcion_xls/index.html', {})


@login_required
def carga(request):

	if request.method == 'POST':
		form = UploadFileForm(request.POST, request.FILES)
	
		if form.is_valid():
			
			new_file = UploadFile(file = request.FILES['file'])
			anho = request.POST['anho']
			sobreescribir = request.POST['sobreescribir']
			print "Sobreescribir",sobreescribir
			#departamento_id = request.POST['departamento']
			#distrito_id = request.POST['distrito']
			anho = int(anho)
			
			print "Año: ",anho
			#print "Departamento ID: ",departamento_id
			#print "Distrito ID: ",distrito_id
			### get the inmemory file
			data = request.FILES.get('file') # get the file from the curl
			
			### write the data to a temp file
			tup = tempfile.mkstemp() # make a tmp file
			f = os.fdopen(tup[0], 'w') # open the tmp file for writing
			f.write(data.read()) # write the tmp file
			f.close()

			### return the path of the file
			filepath = tup[1] # get the filepath
			#Validar el archivo recibido
			if sobreescribir == "true":
				operacion = "sobreescribir"
			else:
				operacion = "normal"
			resultado_validacion_forma = validar_forma.validar(filepath,anho,operacion)
			
			#Prueba de concepto
			#resultado_validacion_forma = {	'valido':False, 
			#								'nroplanilla': 10,
			#								'nombreplanilla': "Sanitarios",
			#								'departamento': "Concepción",
			#								'distrito': 'Azotey',
			#								'log': True,
			#								'errores': "Listado\nDe\nErrores",
			#								'messages':[{'tags':"error",'text':"Planilla no procesada"}]}
			
			
				

			if resultado_validacion_forma['valido']: 
				resultado_validacion_forma["info"] = True
				#Guardar archivo Valido en carpeta correspondiente
				#obtener el departamento,distrito,fecha y tipo de planilla.
				planilla = resultado_validacion_forma["planilla"]
				departamento_obj = resultado_validacion_forma["departamento"]
				distrito_obj = resultado_validacion_forma["distrito"]
				filename = "Planilla"+planilla+"_"+distrito_obj.nombre_jurisdiccion+"_"+departamento_obj.nombre_jurisdiccion+".xls"
				#/planillas/Sanitarios/PlanillaSanitarios_distrito_departamento.xls
				models.set_file_path(planilla, filename)
				new_file.nro_planilla = resultado_validacion_forma["nroplanilla"]
				new_file.anho = anho
				new_file.departamento = departamento_obj.nombre_jurisdiccion
				new_file.distrito = distrito_obj.nombre_jurisdiccion
				new_file.departamento_id = departamento_obj.codigo_jurisdiccion
				new_file.distrito_id = distrito_obj.codigo_jurisdiccion
				new_file.save()
				resultado_validacion_forma['estado_planillas'] = get_estatus_planillas(departamento_obj,distrito_obj,anho)
			else:
				if resultado_validacion_forma["marcado"]:
				#Generar link para el archivo modificado
					nombre_archivo = os.path.basename(resultado_validacion_forma["marcado"])
					resultado_validacion_forma["marcado"] = nombre_archivo
					resultado_validacion_forma["info"] = True


					departamento_obj = resultado_validacion_forma["departamento"]
					distrito_obj = resultado_validacion_forma["distrito"]
					resultado_validacion_forma['estado_planillas'] = get_estatus_planillas(departamento_obj,distrito_obj,anho)
			

				else:
					resultado_validacion_forma["info"] = False
				#Los errores y mensajes ya estan cargados

			#Conseguir el estado de las otras planillas
			#Eliminacion del archivo temporal
			os.unlink(filepath)

			#retornar el html con la informacion sobre la planilla procesada
			#la misma incluye errores en caso de lo que los hubiese
			return render_to_response('recepcion_xls/carga_info.html',resultado_validacion_forma,context_instance=RequestContext(request))
			
			#return render(request,'recepcion_xls/index.html',{'form':form, 'errorlog':errorlog, 'log':log})
			#return HttpResponseRedirect(reverse('recepcion_xls:carga'))
	else:
		
		form = UploadFileForm()
	
	return render(request,'recepcion_xls/carga.html',{'form':form})



@login_required
def lista_tareas(request):
	usuario = request.user
	tareas_usuario = models.Tarea.objects.filter(usuario=usuario)
	
	tareas_disponibles = models.Tarea.objects.filter(estado="No asignada")
	
	#Chequeo de permisos para mostrar ciertas tareas
	

	#Traer tareas para las cuales tiene permiso
	#Filtrar aquellas que no pertenezcan al distrito


	

	return render(request,'recepcion_xls/tareas_asignadas.html',{"tareas_disponibles":tareas_disponibles,"tareas_usuario":tareas_usuario})

@login_required
def asignar_tarea(request,tarea_id):
	usuario = request.user
	tarea =models.Tarea.objects.filter(id_tarea=tarea_id)[0]
	tarea.estado = "Pendiente"
	tarea.usuario = usuario
	tarea.save()
	return HttpResponseRedirect(reverse('recepcion_xls:lista_tareas'))

@login_required
def realizar_tarea(request,tarea_id):
	"""
	Toma un id de tarea, y busca el presentador (vista)
	correspondiente para la misma. Este presentador tiene la 
	interfaz de resolucion de tareas
	"""
	tarea = models.Tarea.objects.filter(id_tarea=tarea_id)[0]
	if tarea.tipo == "Corrección de Códigos":
		return resolver_tarea_codigos(request,tarea_id)
	
	return HttpResponse("Tarea sin presentador")

@login_required
def resolver_tarea_codigos(request,tarea_id):
	#Conseguir la tarea
	tarea = models.Tarea.objects.filter(id_tarea=tarea_id)[0]
	#Que planilla?
	if tarea.nro_planilla in [7,9,10,11]: #
		planilla = models.PlanillaInfraestructura.objects.filter(id=tarea.fila_planilla)[0]

	#if tarea.nro_planilla == 12: #Alimentacion
	#	planilla = models.PlanillaAlimentacion.objects.filter(id=tarea.fila_planilla)[0]
		
	sugerencias = tareas.get_lista_sugerencias(planilla,tarea.nro_planilla)

	if request.method == 'POST':
		form = CorreccionCodigosForm(request.POST)
		
		#Procesar 
		planilla.codigo_institucion = request.POST["codigo_institucion"]
		planilla.codigo_local = request.POST["codigo_local"]
		if validadores_fondo.validar_codigos(planilla,tarea.nro_planilla):
			
			#Corregir la planilla
			planilla.tareas_asociadas = planilla.tareas_asociadas - 1
			planilla.save()

			#Cambiar el estado de la tarea
			tarea.estado = "Completa"
			tarea.version_modificada = planilla.history.all()[0].history_id
			tarea.fecha = timezone.now()
			tarea.save()
			
			#tarea_realizada = models.Tarea_realizada()
			#tarea_realizada.tarea = tarea
			#tarea_realizada.valor_corregido = form['valor_corregido']
			#tarea_realizada.fecha = date.today()
			#tarea_realizada.save()

			#Volvemos a la lista de tareas
			return HttpResponseRedirect(reverse('recepcion_xls:lista_tareas'))

		else:
			messages = []
			if not validadores_fondo.es_cod_institucion_valido(planilla,tarea.nro_planilla):
				messages.append({'text':"La institución no existe. Verifique el código de institución ingresado."})
			if not validadores_fondo.es_cod_local_valido(planilla,tarea.nro_planilla):
				messages.append({'text':"El local no existe. Verifique el código de local ingresado."})
			if not validadores_fondo.cod_inst_en_local(planilla,tarea.nro_planilla):
				messages.append({'text':"La institución no pertenece al local. Verifique el código de institución y de local ingresado."})

			return render(request,'recepcion_xls/resolver_codigos.html',{'form':form, 'tarea':tarea,'planilla':planilla,'sugerencias':sugerencias, 'messages':messages})
	else:
		#Para el GET
		default_data = {'codigo_institucion':planilla.codigo_institucion, 'codigo_local':planilla.codigo_local}
		form = CorreccionCodigosForm(default_data)

		

	return render(request,'recepcion_xls/resolver_codigos.html',{'form':form,'tarea':tarea,'planilla':planilla,'sugerencias':sugerencias})



#API
@login_required
def get_xls_modificado(request,nombre_archivo):
	"""
	Funcion a ser llamada cuando el usuario desee descargar el archivo xls del_xls_modificado
	El nombre sera proveido luego del pro
	"""
	#Recuperar el archivo de tmp
	
	try:
		fsock = open("/tmp/"+nombre_archivo,"rb")
		
		file_name = "PlanillaModificada.xls"
		file_size = os.path.getsize("/tmp/"+nombre_archivo)
		print "file size is: " + str(file_size)
		response = HttpResponse(fsock, content_type="application/vnd.ms-excel")
		response['Content-Disposition'] = 'attachment; filename=' + file_name            
	except IOError:
		response = HttpResponseNotFound()
	
	return response

@login_required
def del_xls_modificado(request,nombre_archivo):
	"""
	Funcion a ser llamada una vez que se complete la descarga
	"""
	#Eliminar del sistema de archivos el archivo tmp ya descargado
	os.unlink("/tmp/"+nombre_archivo)



def get_estatus_planillas(departamento_obj,distrito_obj,anho):
	#Revisar status planillas de infraestructura
	planillas = []
	for planilla in range(len([7,9,10,11,12])):
		planillas.append([None,None])

	planillas[0][0] = False if models.PlanillaInfraestructura.objects.filter(departamento_id=departamento_obj.codigo_jurisdiccion,distrito_id=distrito_obj.codigo_jurisdiccion,nro_planilla=7,anho=anho).count() == 0 else True
	planillas[0][1] = True if models.PlanillaInfraestructura.objects.filter(departamento_id=departamento_obj.codigo_jurisdiccion,distrito_id=distrito_obj.codigo_jurisdiccion,nro_planilla=7,anho=anho,tareas_asociadas__gt=0).count() == 0 else False
	
	planillas[1][0] = False if models.PlanillaInfraestructura.objects.filter(departamento_id=departamento_obj.codigo_jurisdiccion,distrito_id=distrito_obj.codigo_jurisdiccion,nro_planilla=9,anho=anho).count() == 0 else True
	planillas[1][1] = True if models.PlanillaInfraestructura.objects.filter(departamento_id=departamento_obj.codigo_jurisdiccion,distrito_id=distrito_obj.codigo_jurisdiccion,nro_planilla=9,anho=anho,tareas_asociadas__gt=0).count() == 0 else False
	
	planillas[2][0] = False if models.PlanillaInfraestructura.objects.filter(departamento_id=departamento_obj.codigo_jurisdiccion,distrito_id=distrito_obj.codigo_jurisdiccion,nro_planilla=10,anho=anho).count() == 0 else True
	planillas[2][1] = True if models.PlanillaInfraestructura.objects.filter(departamento_id=departamento_obj.codigo_jurisdiccion,distrito_id=distrito_obj.codigo_jurisdiccion,nro_planilla=10,anho=anho,tareas_asociadas__gt=0).count() == 0 else False
	
	planillas[3][0] = False if models.PlanillaInfraestructura.objects.filter(departamento_id=departamento_obj.codigo_jurisdiccion,distrito_id=distrito_obj.codigo_jurisdiccion,nro_planilla=11,anho=anho).count() == 0 else True
	planillas[3][1] = True if models.PlanillaInfraestructura.objects.filter(departamento_id=departamento_obj.codigo_jurisdiccion,distrito_id=distrito_obj.codigo_jurisdiccion,nro_planilla=11,anho=anho,tareas_asociadas__gt=0).count() == 0 else False
	
	
	#Revisar es
	planillas[4][0] = False if models.PlanillaAlimentacion.objects.filter(departamento_id=departamento_obj.codigo_jurisdiccion,distrito_id=distrito_obj.codigo_jurisdiccion,nro_planilla=12,anho=anho).count() == 0 else True
	planillas[4][1] = True if models.PlanillaAlimentacion.objects.filter(departamento_id=departamento_obj.codigo_jurisdiccion,distrito_id=distrito_obj.codigo_jurisdiccion,nro_planilla=12,anho=anho,tareas_asociadas__gt=0).count() == 0 else False

	
	estado_planillas = []
	valid = "check.svg"
	uploaded = "upload.svg"
	no = "mark.svg"
	for planilla in planillas:
		if planilla[0]: #La planilla fue subida
			if planilla[1]: #No tiene tareas pendientes
				estado_planillas.append(valid) #Check
			else:
				estado_planillas.append(uploaded) #Uploaded con tareas pendientes
		else:
			estado_planillas.append(no) # No subida


	return estado_planillas

def recrear_xls(request,departamento,distrito,anho,planilla_nro):
	distrito = departamento + "-" + distrito
	infoArchivo = models.UploadFile.objects.filter(departamento_id=departamento,distrito_id=distrito,anho=anho,nro_planilla=planilla_nro)
	if len(infoArchivo) == 0:
		return HttpResponse("No existe")
	else:
		infoArchivo = infoArchivo[0]
		print BASE_DIR
		print infoArchivo.file.url
		path_archivo = BASE_DIR+infoArchivo.file.url #path
		print path_archivo

		print 
		print infoArchivo.departamento
		#leer_xls
		operacion="regenerar_planilla"
		info_lectura = validar_forma.leer_xls(path_archivo,infoArchivo.anho,operacion)
		#usar el archivo modificado para escribir las filas sacadas de la BD
		wb = info_lectura["libro_para_escritura"]
		inicio_datos = info_lectura["inicio_datos"]
		columna_atributo = info_lectura["atributos"]
		atributo_columna = {v: k for k, v in columna_atributo.items()}
		planilla_nro = info_lectura["planilla"]

		#Sacar filas de la BD
		if planilla_nro in [7,9,10,11]:
			filas = models.PlanillaInfraestructura.objects.filter(nro_planilla=planilla_nro,anho=anho,departamento_id=departamento,distrito_id=distrito)
		else:
			filas = models.PlanillaAlimentacion.objects.filter(nro_planilla=planilla_nro,anho=anho,departamento_id=departamento,distrito_id=distrito)
		
		for i in range(0,len(filas)): #para cada fila extraida de la BD
			row = i + inicio_datos #Se calcula la posicion de la fila teniendo en cuenta el inicio de los datos en la planilla
			for atrib in atributo_columna: #Para cada atributo
				col = atributo_columna[atrib] #Se obtiene la columna donde debe ir el valor del atributo
				valor = getattr(filas[i], atrib) #Se obtiene el valor del atributo
				wb.get_sheet(0).write(row,col,valor) #Se almacena en la fila y columna que corresponda
		
		archivo = StringIO()
		wb.save(archivo)
		
		in_memory = StringIO()  
		zip = ZipFile(in_memory, "a")  
				  
		zip.writestr("Planilla "+str(planilla_nro)+" "+str(anho)+" "+ str(infoArchivo.departamento)+" "+str(infoArchivo.distrito)+".xls", archivo.getvalue())  
			  
		# fix for Linux zip files read in Windows  
		for file in zip.filelist:  
			file.create_system = 0      
				  
		zip.close()  
		  
		response = HttpResponse(content_type="application/zip")  
		response["Content-Disposition"] = "attachment; filename=Planilla.zip"
		  
		in_memory.seek(0)      
		response.write(in_memory.read())  
		  
		return response  
		#archivo_marcado = StringIO.StringIO()
		#Guardar en un stream
		#wb.save(archivo_marcado)
		

def estado_planillas(request):
	return render(request,'recepcion_xls/estado_recepcion.html',{})

def dashboard(request,anho):
	#anho = 2015
	#anho = request.GET["anho"]

	estado_planillas = []
	
	for dpto in models.Jurisdiccion.objects.using("mec_public").filter(numero_nivel_jurisdiccion=2):
		for dist in models.Jurisdiccion.objects.using("mec_public").filter(numero_nivel_jurisdiccion=3,id_jurisdiccion_superior=dpto.id_jurisdiccion):
			estado_planillas.append({
				"distrito":dist.nombre_jurisdiccion,
				"departamento": dpto.nombre_jurisdiccion,
				"distrito_id":dist.codigo_jurisdiccion.split("-")[1],
				"departamento_id":dpto.codigo_jurisdiccion,
				"anho":anho,
				"estado": get_estatus_planillas(dpto,dist,anho)
				})

		
	return render(request,'recepcion_xls/estado_recepcion_info.html',{"estado_planillas":estado_planillas,"sucess":True})

def get_departamentos(request):
	departamentos = models.Jurisdiccion.objects.using("mec_public").filter(numero_nivel_jurisdiccion=2).order_by("codigo_jurisdiccion")
	departamentosJSON = []
	for dpto in departamentos:
		print dpto.codigo_jurisdiccion,dpto.nombre_jurisdiccion
		departamentosJSON.append([dpto.codigo_jurisdiccion,dpto.codigo_jurisdiccion+" - "+dpto.nombre_jurisdiccion])
	return JsonResponse({"lista_departamentos":departamentosJSON})

@csrf_exempt
def get_distritos(request):
	distritosJSON = []
	if "depdrop_parents[0]" in request.POST.keys() and int(request.POST['depdrop_parents[0]']) != -1:
		dpto_id = request.POST['depdrop_parents[0]']
		departamento = models.Jurisdiccion.objects.using("mec_public").filter(numero_nivel_jurisdiccion=2,codigo_jurisdiccion=dpto_id)[0]
		distritos = models.Jurisdiccion.objects.using("mec_public").filter(numero_nivel_jurisdiccion=3,id_jurisdiccion_superior=departamento.id_jurisdiccion).order_by("codigo_jurisdiccion")
		distritosJSON.append({"id":"-1","name":"Todos los distritos"})
		for distrito in distritos:
			distritosJSON.append({"id":distrito.codigo_jurisdiccion.split("-")[1],"name":distrito.codigo_jurisdiccion.split("-")[1]+" - "+distrito.nombre_jurisdiccion})
		
		return JsonResponse({"output": distritosJSON, "selected": ""})
		
	else:
		return JsonResponse({"output": [], "selected": ""})



def generar_xls(departamento_obj,distrito_obj,anho,planilla_nro):
	infoArchivo = models.UploadFile.objects.filter(departamento_id=departamento_obj.codigo_jurisdiccion,distrito_id=distrito_obj.codigo_jurisdiccion,anho=anho,nro_planilla=planilla_nro)
	if len(infoArchivo) == 0:
		return None
	else:
		infoArchivo = infoArchivo[0]
		#print BASE_DIR
		#print infoArchivo.file.url
		path_archivo = BASE_DIR+infoArchivo.file.url #path
		#print path_archivo

		#print infoArchivo.departamento
		#leer_xls
		operacion="regenerar_planilla"
		info_lectura = validar_forma.leer_xls(path_archivo,infoArchivo.anho,operacion)
		#usar el archivo modificado para escribir las filas sacadas de la BD
		wb = info_lectura["libro_para_escritura"]
		inicio_datos = info_lectura["inicio_datos"]
		columna_atributo = info_lectura["atributos"]
		atributo_columna = {v: k for k, v in columna_atributo.items()}
		planilla_nro = info_lectura["planilla"]

		#Sacar filas de la BD
		if planilla_nro in [7,9,10,11]:
			filas = models.PlanillaInfraestructura.objects.filter(nro_planilla=planilla_nro,anho=anho,departamento_id=departamento_obj.codigo_jurisdiccion,distrito_id=distrito_obj.codigo_jurisdiccion)
		else:
			filas = models.PlanillaAlimentacion.objects.filter(nro_planilla=planilla_nro,anho=anho,departamento_id=departamento_obj.codigo_jurisdiccion,distrito_id=distrito_obj.codigo_jurisdiccion)
		
		for i in range(0,len(filas)): #para cada fila extraida de la BD
			row = i + inicio_datos #Se calcula la posicion de la fila teniendo en cuenta el inicio de los datos en la planilla
			for atrib in atributo_columna: #Para cada atributo
				col = atributo_columna[atrib] #Se obtiene la columna donde debe ir el valor del atributo
				valor = getattr(filas[i], atrib) #Se obtiene el valor del atributo
				wb.get_sheet(0).write(row,col,valor) #Se almacena en la fila y columna que corresponda
		
		archivo = StringIO()
		wb.save(archivo)

	return archivo






def add_planillas_zip(zip_file,departamento_obj,distrito_obj,anho):
	#7
	planilla_7 = generar_xls(departamento_obj,distrito_obj,anho,7)
	if planilla_7 != None:
		zip_file.writestr(departamento_obj.nombre_jurisdiccion+"/"+distrito_obj.nombre_jurisdiccion+"/"+"Planilla_7.xls",planilla_7.getvalue())
	#9
	planilla_9 = generar_xls(departamento_obj,distrito_obj,anho,9)
	if planilla_9 != None:
		zip_file.writestr(departamento_obj.nombre_jurisdiccion+"/"+distrito_obj.nombre_jurisdiccion+"/"+"Planilla_9.xls",planilla_9.getvalue())
	
	#10
	planilla_10 = generar_xls(departamento_obj,distrito_obj,anho,10)
	if planilla_10 != None:
		zip_file.writestr(departamento_obj.nombre_jurisdiccion+"/"+distrito_obj.nombre_jurisdiccion+"/"+"Planilla_10.xls",planilla_10.getvalue())
	
	#11
	planilla_11 = generar_xls(departamento_obj,distrito_obj,anho,11)
	if planilla_11 != None:
		zip_file.writestr(departamento_obj.nombre_jurisdiccion+"/"+distrito_obj.nombre_jurisdiccion+"/"+"Planilla_11.xls",planilla_11.getvalue())
	
	#12
	planilla_12 = generar_xls(departamento_obj,distrito_obj,anho,12)
	if planilla_12 != None:
		zip_file.writestr(departamento_obj.nombre_jurisdiccion+"/"+distrito_obj.nombre_jurisdiccion+"/"+"Planilla_12.xls",planilla_12.getvalue())





def descargar_zip(request):
	anho = request.GET["anho"]
	departamento_id = None
	distrito_id = None
	if "departamento_id" in request.GET.keys():
		departamento_id = request.GET["departamento_id"]
	if "distrito_id" in request.GET.keys():
		distrito_id = request.GET["distrito_id"]
	
	print "Departamento ID  ",type(departamento_id)
	print "Distrito ID  ",type(distrito_id)
	#Creamos el zip
	in_memory = StringIO()  
	zip_file = ZipFile(in_memory, "a")
		
	#Todos los departamentos
	if int(departamento_id) == -1:
		departamentos = models.Jurisdiccion.objects.using("mec_public").filter(numero_nivel_jurisdiccion=2).order_by("codigo_jurisdiccion")
		print "LEN Dpto: ",len(departamentos)
		for departamento in departamentos:
			print departamento.nombre_jurisdiccion
			for distrito in models.Jurisdiccion.objects.using("mec_public").filter(numero_nivel_jurisdiccion=3,id_jurisdiccion_superior=departamento.id_jurisdiccion):
				#generar archivo
				add_planillas_zip(zip_file,departamento,distrito,anho)
				
	#Todos los distritos del departamento
	elif int(distrito_id) == -1:
		departamento = models.Jurisdiccion.objects.using("mec_public").filter(numero_nivel_jurisdiccion=2,codigo_jurisdiccion=departamento_id)[0]
	
		#Todos los distritos del departamento
	
		for distrito in models.Jurisdiccion.objects.using("mec_public").filter(numero_nivel_jurisdiccion=3,id_jurisdiccion_superior=departamento.id_jurisdiccion):
			#generar archivo
			#añadir al zip
			add_planillas_zip(zip_file,departamento,distrito,anho)
	else:
	#Solo un distrito en particular
		departamento = models.Jurisdiccion.objects.using("mec_public").filter(numero_nivel_jurisdiccion=2,codigo_jurisdiccion=departamento_id)[0]
		distrito = models.Jurisdiccion.objects.using("mec_public").filter(numero_nivel_jurisdiccion=3,id_jurisdiccion_superior=departamento.id_jurisdiccion,codigo_jurisdiccion=distrito_id)[0]
		add_planillas_zip(zip_file,departamento,distrito,anho)

	#Un distrito en particular



	zip_file.close()

	response = HttpResponse(content_type="application/zip")  
	response["Content-Disposition"] = "attachment; filename=Planillas.zip"
		  
	in_memory.seek(0)      
	response.write(in_memory.read())  
		  
	return response

