#coding: UTF-8
from django import forms
from django.contrib.auth.models import User
from captcha.fields import ReCaptchaField
from models import UploadFile, Tarea, Jurisdiccion, UserProfile
from registration.forms import RegistrationForm
from recepcion_xls import models

class UserReCaptchaForm(RegistrationForm):
	captcha = ReCaptchaField()
	

class UserProfileForm(forms.ModelForm):
	distrito = forms.ModelChoiceField(queryset=models.Jurisdiccion.objects.using("mec_public").filter(numero_nivel_jurisdiccion=3),
                                   required=False)
	class Meta:
		model = UserProfile
		fields = ('user','distrito',)

class UserForm(forms.ModelForm):
	password = forms.CharField(widget=forms.PasswordInput())

	class Meta:
		model = User
		fields = ('username', 'email', 'password')


class UploadFileForm(forms.ModelForm):
	class Meta:
		model = UploadFile
		fields = ('file',)

class CorreccionCodigosForm(forms.Form):
	codigo_institucion = forms.CharField(label='Código de Institución')
	codigo_local = forms.CharField(label='Código de Local')

