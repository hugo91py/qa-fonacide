from django.contrib import admin
from recepcion_xls import models
from recepcion_xls.forms import UserProfileForm
from simple_history.admin import SimpleHistoryAdmin
from django import forms

class UserProfileAdmin(admin.ModelAdmin):
	form = UserProfileForm


admin.site.register(models.PlanillaInfraestructura, SimpleHistoryAdmin)
admin.site.register(models.PlanillaAlimentacion, SimpleHistoryAdmin)
# Register your models here.

admin.site.register(models.UserProfile, UserProfileAdmin)