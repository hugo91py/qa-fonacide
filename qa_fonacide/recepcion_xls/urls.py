from django.conf.urls import patterns, include, url

urlpatterns = patterns(
						'',
						url(r'^$','recepcion_xls.views.index',name='index'),
						url(r'^carga/$','recepcion_xls.views.carga',name='carga'),
						url(r'^descargar_modificado/(?P<nombre_archivo>\w+)/$','recepcion_xls.views.get_xls_modificado',name='descargar_modificado'),
						url(r'^descargar_xls/(\w+)/(\w+)/(\w+)/(\w+)/$','recepcion_xls.views.recrear_xls',name='recrear_xls'),
						url(r'^estado/$','recepcion_xls.views.estado_planillas',name='estado_planillas'),
						url(r'^dashboard/(\w+)/$','recepcion_xls.views.dashboard',name='dashboard'),
						url(r'^get_departamentos/$','recepcion_xls.views.get_departamentos',name='get_departamentos'),
						url(r'^get_distritos/$','recepcion_xls.views.get_distritos',name='get_distritos'),
						url(r'^descargar_zip/$','recepcion_xls.views.descargar_zip',name='descargar_zip'),
						#url(r'^descargar_xls/(?P<departamento>\w+)/(?P<distrito>\w+)/(?P<anho>\w+)/$','recepcion_xls.views.recrear_xls',name='recrear_xls'),
						url(r'^tareas/$','recepcion_xls.views.lista_tareas',name='lista_tareas'),
						url(r'^tareas/(?P<tarea_id>\d+)/$','recepcion_xls.views.realizar_tarea',name='realizar_tarea'),
						url(r'^asignar_tarea/(?P<tarea_id>\d+)/$','recepcion_xls.views.asignar_tarea',name='asignar_tarea'),
						url(r'^registro/$','recepcion_xls.views.register', name='register'),
					  	url(r'^login/$','recepcion_xls.views.user_login', name='login'),
					  	url(r'^logout/$','recepcion_xls.views.user_logout', name='logout')
					  )

